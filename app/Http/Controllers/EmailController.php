<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\DetailKonsultasi;
use App\Mail\ResetPasswordMail;
use App\Models\Konsultasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function kirim(Request $request)
    {
        $konsultasi = Konsultasi::with(['detail', 'user'])
            ->where('id', $request->id)
            ->first();

        $dt = [
            'token' => $request->_token,
            'email' => $konsultasi->user->email,
            'data' => $konsultasi
        ];

        $send = Mail::to($konsultasi->user->email)->send(new DetailKonsultasi($dt));

        if ($send) {
            $res = [
                'status' => true,
                'pesan' => 'Pesan sudah dikirimkan ke '.$konsultasi->user->email.'. Silahkan di cek.',
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => 'Gagal Mengirimkan Pesan.',
            ];
        }

        // if ($send) {
        //     session()->flash('success', 'Pesan sudah dikirimkan ke '.$konsultasi->user->email.'. Silahkan di cek.');
        // } else {
        //     session()->flash('error', 'Gagal Mengirimkan Pesan');
        // }
        return response()->json($res);
        // return redirect()->route('konsultasi');
    }
}
