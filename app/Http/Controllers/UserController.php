<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Alamatuser;
use App\Models\cart;
use App\Models\t_kab;
use App\Models\t_kec;
use App\Models\t_provinsi;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Dashboard',
            'menu' => 'Dashboard',
            'li_active' => 'dashboard',
        ];
        return view('users/dashboard', $data);
    }

    function profil()
    {
        $ref_provinsi = t_provinsi::all();
        $alamat = Alamatuser::where('id_user', Auth::user()->id)
            ->whereNull('deleted_at')
            ->first();
        $data = [
            'title' => 'Profil Customer',
            'li_active' => 'profil',
            'alamat' => $alamat,
            'ref_provinsi' => $ref_provinsi,
            'script_js' => 'customer/profil/profil.js',
        ];
        return view('customer/profil', $data);
    }

    function simpan(Request $request)
    {
        $request->validate(
            [
                'email' => 'email',
            ],
            [
                'email.email' => 'Format Email anda salah.',
            ],
        );

        $dt = [
            'username' => $request->username,
            'name' => $request->nama,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ];
        if (!empty($request->provinsi)) {
            $provinsi = t_provinsi::where('province_id', $request->provinsi)->first();
        }
        if (!empty($request->kabupaten)) {
            $kabupaten = t_kab::where('city_id', $request->kabupaten)->first();
        }
        if (!empty($request->kecamatan)) {
            $kecamatan = t_kec::where('subdistrict_id', $request->kecamatan)->first();
        }
        $dt_alamat = [
            'id_user' => Auth::user()->id,
            'province_id' => $request->provinsi,
            'city_id' => @$request->kabupaten,
            'subdistrict_id' => @$request->kecamatan,
            'provinsi' => @$provinsi->province_name,
            'kabupaten' => @$kabupaten->city_name,
            'kecamatan' => @$kecamatan->subdistrict_name,
            'detail_alamat' => @$request->alamat,
        ];
        User::where('id', Auth::user()->id)->update($dt);
        if (!empty($request->id_alamat)) {
            $dt_alamat['updated_at'] = now()->format('Y-m-d H:i:s');
            $insert = Alamatuser::where('id', $request->id_alamat)->update($dt_alamat);
        } else {
            $dt_alamat['created_at'] = now()->format('Y-m-d H:i:s');
            $insert = Alamatuser::insert($dt_alamat);
        }

        if ($insert) {
            session()->flash('success', 'Profil berhasil di simpan.');
        } else {
            session()->flash('error', 'Profil gagal di simpan.');
        }
        return redirect()->route('profil_user');
    }

    function get_kabupaten(Request $request)
    {
        $provinsi_id = $request->province_id;
        $ref_kab = t_kab::where('province_id', $provinsi_id)->get();
        return json_encode($ref_kab);
    }

    function get_kecamatan(Request $request)
    {
        $kab_id = $request->city_id;
        $ref_kec = t_kec::where('city_id', $kab_id)->get();
        return json_encode($ref_kec);
    }

    function keranjang()
    {
        $data = [
            'title' => 'Keranjang | Basecampidn.',
            'li_active' => 'keranjang',
        ];
        return view('front/keranjang', $data);
    }

    function cek()
    {
        $date = '2023-01-01 11:44:45';
        $data = [
            'cek' => Carbon::parse($date)->isoFormat('dddd, D MMMM Y'),
        ];
        return view('front/cek', $data);
    }
}
