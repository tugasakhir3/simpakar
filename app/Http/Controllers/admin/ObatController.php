<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Gejala;
use App\Models\Kategori;
use App\Models\Obat;
use App\Models\Paket;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ObatController extends Controller
{
    public function index()
    {
        $dt_user = DB::table('users as a')->leftJoin('alamatuser as b', 'a.id', '=', 'b.id_user')->leftJoin('ref_provinsi as c', 'b.province_id', '=', 'c.province_id')->leftJoin('ref_kab as d', 'b.city_id', '=', 'd.city_id')->leftJoin('ref_kec as e', 'b.subdistrict_id', '=', 'e.subdistrict_id')->select('a.*', 'c.province_name', 'd.city_name', 'e.subdistrict_name', 'b.detail_alamat')->where('role', 'admin')->whereNull('a.deleted_at')->get();

        $data = [
            'title' => 'Referensi Obat',
            'menu' => 'Referensi Obat',
            'li_active' => 'obat',
            'dt_user' => $dt_user,
            'jml' => count($dt_user),
        ];
        return view('admin/obat/index', $data);
    }

    public function dataObat()
    {
        $query = Obat::select('*'); // Select all columns from Paket model
        $data = $query->get();
        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    function hapus(Request $request)
    {
        $id = $request->id;
        $insert = Obat::where('id', $id)->delete();
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => 'Berhasil Menghapus Obat.',
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => 'Gagal Menghapus Obat.',
            ];
        }

        echo json_encode($res);
    }

    function simpan(Request $request)
    {
        $id = $request->input('id');
        $request->validate(
            [
                'obat' => 'required',
            ],
            [
                'obat.required' => 'Nama Obat Tidak Boleh Kosong',
            ],
        );

        $dt = [
            'obat' => $request->obat,
            'keterangan' => $request->keterangan,
        ];

        if ($id) {
            $dt['updated_at'] = now()->format('Y-m-d H:i:s');
            $insert = Obat::where('id', $id)->update($dt);
            $pesan = 'di edit';
        } else {
            $dt['created_at'] = now()->format('Y-m-d H:i:s');
            $insert = Obat::insert($dt);
            $pesan = 'di tambahkan';
        }

        if ($insert) {
            session()->flash('success', 'Obat berhasil ' . $pesan . '.');
        } else {
            session()->flash('error', 'Obat gagal ' . $pesan . '.');
        }

        return redirect()->route('obat');
    }

    function upload_foto($path, $file)
    {
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Obat_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path . '/' . $namaFile;
    }
}
