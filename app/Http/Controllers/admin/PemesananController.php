<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PemesananController extends Controller
{
    public function daftar_admin()
    {
        $dt_user = DB::table('users as a')
            ->leftJoin('alamatuser as b', 'a.id', '=', 'b.id_user')
            ->leftJoin('ref_provinsi as c', 'b.province_id', '=', 'c.province_id')
            ->leftJoin('ref_kab as d', 'b.city_id', '=', 'd.city_id')
            ->leftJoin('ref_kec as e', 'b.subdistrict_id', '=', 'e.subdistrict_id')
            ->select('a.*', 'c.province_name', 'd.city_name', 'e.subdistrict_name', 'b.detail_alamat')
            ->where('role','admin')
            ->whereNull('a.deleted_at')
            ->get();

        $data = [
            'page_title' => 'Daftar Admin',
            'li_active' => 'manajemen_user',
            'li_sub_active' => 'daftar_admin',
            'dt_user' => $dt_user,
            'jml' => count($dt_user),
            'script_js' => 'admin/users/admin.js',
        ];
        return view('admin/users/admin', $data);
    }

    public function daftar_customer()
    {
        $dt_user = DB::table('users as a')
            ->leftJoin('alamatuser as b', 'a.id', '=', 'b.id_user')
            ->leftJoin('ref_provinsi as c', 'b.province_id', '=', 'c.province_id')
            ->leftJoin('ref_kab as d', 'b.city_id', '=', 'd.city_id')
            ->leftJoin('ref_kec as e', 'b.subdistrict_id', '=', 'e.subdistrict_id')
            ->select('a.*', 'c.province_name', 'd.city_name', 'e.subdistrict_name', 'b.detail_alamat')
            ->whereNot('role','admin')
            ->whereNull('a.deleted_at')
            ->get();

        $data = [
            'page_title' => 'Daftar Customer',
            'li_active' => 'manajemen_user',
            'li_sub_active' => 'daftar_customer',
            'dt_user' => $dt_user,
            'script_js' => 'admin/users/customer.js',
        ];
        return view('admin/users/customer', $data);
    }

    function hapus(Request $request){
        $id = $request->id;
        $dt = ['deleted_at' => now()->format('Y-m-d H:i:s')];
        $insert = User::where('id',$id)->update($dt);
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => "Berhasil Menghapus Admin."
            ];
        }else{
            $res = [
                'status' => false,
                'pesan' => "Gagal Menghapus Admin."
            ];
        }

        echo json_encode($res);
    }

    function tambah_admin(Request $request){

        $request->validate([
            'username' => ['required','unique:'.User::class],
            'nama' => ['required'],
            'email' => ['required', 'email','unique:'.User::class],
            'password' => ['required'],
        ],[
            'username.required'=>'Username tidak boleh kosong.',
            'username.unique'=>'Username Sudah Digunakan.',
            'nama.required'=>'Nama tidak boleh kosong.',
            'email.required'=>'Email tidak boleh kosong.',
            'email.unique'=>'Email Sudah Digunakan.',
            'password.required'=>'Password tidak boleh kosong.',
        ]);

        $dt = [
            'username' => $request->username,
            'name' => $request->nama,
            'email' => $request->email,
            'password'=> Hash::make($request->password),
            'reset_id'=> $request->password,
            'role'=> 'admin'
        ];
        
        $insert = User::insert($dt);
        
        if ($insert) {
            session()->flash('success', 'Admin berhasil di tambah.');
        } else {
            session()->flash('error', 'Admin gagal di tambah.');
        }
        return redirect()->route('daftar_admin');
    }

   
    public function tambah_kaos()
    {
        $data = [
            'page_title' => 'Tambah Kaos',
            'li_active' => 'produk',
            'li_sub_active' => 'kelola_kaos',
            'script_js' => 'admin/users/admin.js',
        ];
        return view('admin/users/adminah_kaos', $data);
    }

    public function tambah_produk()
    {
        $ref_kategori = Kategori::all();
        $data = [
            'title' => 'Tambah Produk',
            'page_title' => 'Tambah Produk',
            'li_active' => 'produk',
            'li_sub_active' => 'kelola_kaos',
            'ref_kategori' => $ref_kategori,
            'script_js' => 'admin/users/adminuk.js',
        ];
        return view('admin/users/adminah_produk', $data);
    }

    public function edit_produk($id)
    {
        $produk = t_produk::where('id',$id)->first();
        $ref_kategori = Kategori::all();
        $data = [
            'title' => 'Tambah Produk',
            'page_title' => 'Tambah Produk',
            'li_active' => 'produk',
            'li_sub_active' => 'kelola_kaos',
            'produk' => $produk,
            'ref_kategori' => $ref_kategori,
            'script_js' => 'admin/users/adminuk.js',
        ];
        return view('admin/users/adminah_produk', $data);
    }

    function simpan(Request $request)
    {
        $request->validate(
            [
                'produk' => 'required',
                'deskripsi' => 'required',
                'harga' => 'required',
                'kategori' => 'required',
                'foto' => 'image|mimes:jpeg,png,jpg|max:5048', // Validasi tipe dan ukuran file
            ],
            [
                'produk.required' => "Nama Produk Tidak Boleh Kosong",
                'deskripsi.required' => "Deskripsi Tidak Boleh Kosong",
                'harga.required' => "Harga Tidak Boleh Kosong",
                'kategori.required' => "Kategori Tidak Boleh Kosong",
                'foto.image' => 'File harus berupa gambar.',
                'foto.mimes' => 'Format file harus jpeg, png, atau jpg.',
                'foto.max' => 'Ukuran file tidak boleh melebihi 5 MB.',
            ],
        );
        $kategori = Kategori::where('id',$request->kategori)->first()->kategori;
        if ($kategori == "Kaos") { $path = 'produk/kaos';
        } else { $path = 'produk/tas';}
        
        $harga = str_replace(['Rp', '.', ' '], '', $request->harga);
        
        $dt = [
            'id_kategori'=>$request->kategori,
            'nama_produk'=>$request->produk,
            'deskripsi'=>$request->deskripsi,
            'harga'=> intval($harga),
            'stok' => $request->stok
        ];

        $file = $request->file('foto');
        if (@$file != null) {
            $file_foto = $this->upload_foto($path, $file);
            $dt['foto'] = @$file_foto;
        }
        if (!empty($request->id)) {
            $dt['updated_at'] = now()->format('Y-m-d H:i:s');
            $insert = t_produk::where('id',$request->id)->update($dt);
        }else{
            $dt['created_at'] = now()->format('Y-m-d H:i:s');
            $insert = t_produk::insert($dt);
        }

        if ($insert) {
            session()->flash('success', 'Produk berhasil di tambahkan.');
        }else{
            session()->flash('error', 'Produk gagal di tambahkan.');
        }
        
        if ($kategori == "Kaos") { 
            return redirect()->route('kelola_kaos');
        } else { 
           return redirect()->route('kelola_tas');
        }
    }

    function upload_foto($path, $file){
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Produk_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path .'/'. $namaFile;
    }
}
