<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Penyakit;
use App\Models\Kategori;
use App\Models\Paket;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class PenyakitController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Referensi Penyakit',
            'menu' => 'Referensi Penyakit',
            'li_active' => 'penyakit',
        ];
        return view('admin/penyakit/index', $data);
    }

    public function dataPenyakit()
    {
        $query = Penyakit::select('*'); // Select all columns from Paket model
        $data = $query->get();
        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    function hapus(Request $request)
    {
        $id = $request->id;
        $insert = Penyakit::where('id', $id)->delete();
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => 'Berhasil Menghapus Penyakit.',
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => 'Gagal Menghapus Penyakit.',
            ];
        }

        echo json_encode($res);
    }

    // function tambah_admin(Request $request)
    // {
    //     $request->validate(
    //         [
    //             'username' => ['required', 'unique:' . User::class],
    //             'nama' => ['required'],
    //             'email' => ['required', 'email', 'unique:' . User::class],
    //             'password' => ['required'],
    //         ],
    //         [
    //             'username.required' => 'Username tidak boleh kosong.',
    //             'username.unique' => 'Username Sudah Digunakan.',
    //             'nama.required' => 'Nama tidak boleh kosong.',
    //             'email.required' => 'Email tidak boleh kosong.',
    //             'email.unique' => 'Email Sudah Digunakan.',
    //             'password.required' => 'Password tidak boleh kosong.',
    //         ],
    //     );

    //     $dt = [
    //         'username' => $request->username,
    //         'name' => $request->nama,
    //         'email' => $request->email,
    //         'password' => Hash::make($request->password),
    //         'reset_id' => $request->password,
    //         'role' => 'admin',
    //     ];

    //     $insert = User::insert($dt);

    //     if ($insert) {
    //         session()->flash('success', 'Admin berhasil di tambah.');
    //     } else {
    //         session()->flash('error', 'Admin gagal di tambah.');
    //     }
    //     return redirect()->route('daftar_admin');
    // }

    // public function tambah()
    // {
    //     $data = [
    //         'title' => 'Referensi Penyakit',
    //         'menu' => 'Referensi',
    //         'li_active' => 'Penyakit',
    //     ];
    //     return view('admin/penyakit/tambah', $data);
    // }

    // public function edit($id)
    // {
    //     $paket = Paket::where('id', $id)->first();
    //     $data = [
    //         'title' => 'Manajemen Paket',
    //         'menu' => 'Manajemen',
    //         'li_active' => 'paket',
    //         'paket' => $paket,
    //         'id' => $id,
    //     ];
    //     return view('admin/penyakit/tambah', $data);
    // }

    // public function tambah_kaos()
    // {
    //     $data = [
    //         'page_title' => 'Tambah Kaos',
    //         'li_active' => 'produk',
    //         'li_sub_active' => 'kelola_kaos',
    //         'script_js' => 'admin/users/admin.js',
    //     ];
    //     return view('admin/users/adminah_kaos', $data);
    // }

    // public function tambah_produk()
    // {
    //     $ref_kategori = Kategori::all();
    //     $data = [
    //         'title' => 'Tambah Produk',
    //         'page_title' => 'Tambah Produk',
    //         'li_active' => 'produk',
    //         'li_sub_active' => 'kelola_kaos',
    //         'ref_kategori' => $ref_kategori,
    //         'script_js' => 'admin/users/adminuk.js',
    //     ];
    //     return view('admin/users/adminah_produk', $data);
    // }

    // public function edit_produk($id)
    // {
    //     $produk = t_produk::where('id', $id)->first();
    //     $ref_kategori = Kategori::all();
    //     $data = [
    //         'title' => 'Tambah Produk',
    //         'page_title' => 'Tambah Produk',
    //         'li_active' => 'produk',
    //         'li_sub_active' => 'kelola_kaos',
    //         'produk' => $produk,
    //         'ref_kategori' => $ref_kategori,
    //         'script_js' => 'admin/users/adminuk.js',
    //     ];
    //     return view('admin/users/adminah_produk', $data);
    // }

    function simpan(Request $request)
    {
        $id = $request->input('id');
        $request->validate(
            [
                'penyakit' => 'required',
                'kode_penyakit' => 'required',
            ],
            [
                'penyakit.required' => 'Nama Penyakit Tidak Boleh Kosong',
                'kode_penyakit.required' => 'Nama Penyakit Tidak Boleh Kosong',
            ],
        );

        $dt = [
            'penyakit' => $request->penyakit,
            'kode_penyakit' => $request->kode_penyakit,
            'keterangan' => $request->keterangan,
            'solusi' => $request->solusi,
        ];

        if ($id) {
            $dt['updated_at'] = now()->format('Y-m-d H:i:s');
            $insert = Penyakit::where('id', $id)->update($dt);
            $pesan = 'di edit';
        } else {
            $dt['created_at'] = now()->format('Y-m-d H:i:s');
            $insert = Penyakit::insert($dt);
            $pesan = 'di tambahkan';
        }

        if ($insert) {
            session()->flash('success', 'Penyakit berhasil ' . $pesan . '.');
        } else {
            session()->flash('error', 'Penyakit gagal ' . $pesan . '.');
        }

        return redirect()->route('penyakit');
    }

    function upload_foto($path, $file)
    {
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Paket_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path . '/' . $namaFile;
    }
}
