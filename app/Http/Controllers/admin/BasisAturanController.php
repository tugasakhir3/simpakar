<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\BasisAturan;
use App\Models\BasisAturanObat;
use App\Models\Gejala;
use App\Models\Penyakit;
use App\Models\Kategori;
use App\Models\Obat;
use App\Models\Paket;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class BasisAturanController extends Controller
{
    public function index()
    {
        // $dt_user = DB::table('users as a')->leftJoin('alamatuser as b', 'a.id', '=', 'b.id_user')->leftJoin('ref_provinsi as c', 'b.province_id', '=', 'c.province_id')->leftJoin('ref_kab as d', 'b.city_id', '=', 'd.city_id')->leftJoin('ref_kec as e', 'b.subdistrict_id', '=', 'e.subdistrict_id')->select('a.*', 'c.province_name', 'd.city_name', 'e.subdistrict_name', 'b.detail_alamat')->where('role', 'admin')->whereNull('a.deleted_at')->get();
        $data_penyakit = Penyakit::whereNotIn('id', function ($query) {
            $query->select('id_penyakit')->from('basis_aturan');
        })->get();
        $data = [
            'title' => 'Basis Aturan',
            'menu' => 'Basis Aturan',
            'li_active' => 'basis_aturan',
            'data_penyakit' => $data_penyakit,
            // 'dt_user' => $dt_user,
            // 'jml' => count($dt_user),
        ];
        return view('admin/basis_aturan/index', $data);
    }

    public function dataBasisAturan()
    {
        $query = BasisAturan::select('id_penyakit')->with(['penyakit']);
        $query->groupBy('id_penyakit');
        $data = $query->get();

        foreach ($data as $key) {
            $key->gejala = DB::select(
                'SELECT a.*, c.gejala
                 FROM basis_aturan a
                 JOIN ref_penyakit b ON a.id_penyakit = b.id
                 JOIN ref_gejala c ON a.id_gejala = c.id
                 WHERE a.id_penyakit = ?',
                [$key->id_penyakit],
            );

            $key->obat = DB::select(
                'SELECT a.*, c.obat
                 FROM basis_aturan_obat a
                 JOIN ref_penyakit b ON a.id_penyakit = b.id
                 JOIN ref_obat c ON a.id_obat = c.id
                 WHERE a.id_penyakit = ?',
                [$key->id_penyakit],
            );
        }

        $data = $data->map(function ($item, $index) {
            return [
                'id_penyakit' => $item->id_penyakit,
                'penyakit' => $item->penyakit->penyakit,
                'solusi' => $item->penyakit->solusi,
                'keterangan' => $item->penyakit->keterangan,
                'gejala' => $item->gejala,
                'obat' => $item->obat,
            ];
        });

        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    public function getDataGejala(Request $request)
    {
        $arr_gejala = [];
        if ($request->id_penyakit != null) {
            $arr_gejala = BasisAturan::where('id_penyakit', $request->id_penyakit)->pluck('id_gejala')->toArray();
        }
        $data = Gejala::all();

        $data = $data->map(function ($item, $index) use($arr_gejala) {
            $checked = in_array($item->id, $arr_gejala) ? 'checked' : '';
            return [
                'id' => $item->id,
                'gejala' => $item->gejala,
                'checked' => $checked
            ];
        });

        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    public function getDataObat(Request $request)
    {
        $arr_obat = [];
        if ($request->id_penyakit != null) {
            $arr_obat = BasisAturanObat::where('id_penyakit', $request->id_penyakit)->pluck('id_obat')->toArray();
        }
        $data = Obat::all();

        $data = $data->map(function ($item, $index) use($arr_obat) {
            $checked = in_array($item->id, $arr_obat) ? 'checked' : '';
            return [
                'id' => $item->id,
                'obat' => $item->obat,
                'checked' => $checked
            ];
        });

        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    function hapus(Request $request)
    {
        $id_penyakit = $request->id_penyakit;
        $insert = BasisAturan::where('id_penyakit', $id_penyakit)->delete();
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => 'Berhasil Menghapus Basis Aturan.',
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => 'Gagal Menghapus Basis Aturan.',
            ];
        }

        echo json_encode($res);
    }

    function simpan(Request $request)
    {
        $id = $request->input('id');
        $arr_gejala = $request->input('arr_gejala');
        $arr_obat = $request->input('arr_obat');
        $penyakit = $request->input('penyakit');
        if ($arr_gejala == '[]' || $arr_gejala == '' || $arr_obat == '[]' || $arr_obat == '') {
            if ($arr_gejala == '[]' || $arr_gejala == '') {
                session()->flash('error', 'Wajib menambahkan gejala.');
            } else {
                session()->flash('error', 'Wajib menambahkan obat.');
            }
            
        } else {
            $dt = [];
            $dt_obat = [];
            $array = explode(',', $arr_gejala);
            $array_obat = explode(',', $arr_obat);
            $jml_arr = count($array);
            $jml_arr_obat = count($array_obat);
            for ($i=0; $i < $jml_arr ; $i++) { 
                $dt[] = [
                    'id_penyakit' => $penyakit,
                    'id_gejala' => $array[$i],
                    'created_at'=> now()->format('Y-m-d H:i:s')
                ];
            }
            for ($i=0; $i < $jml_arr_obat ; $i++) { 
                $dt_obat[] = [
                    'id_penyakit' => $penyakit,
                    'id_obat' => $array_obat[$i],
                    'created_at'=> now()->format('Y-m-d H:i:s')
                ];
            }

            BasisAturan::where('id_penyakit',$penyakit)->delete();
            $insert = BasisAturan::insert($dt);
            BasisAturanObat::where('id_penyakit',$penyakit)->delete();
            $insert = BasisAturanObat::insert($dt_obat);
            $pesan = 'di tambahkan';

            if ($insert) {
                session()->flash('success', 'Basis Aturan berhasil ' . $pesan . '.');
            } else {
                session()->flash('error', 'Basis Aturan gagal ' . $pesan . '.');
            }
        }

        return redirect()->route('basisAturan');
    }

    function getDataGejalaEdit(Request $request){
        $arr_gejala = BasisAturan::where('id_penyakit', $request->id_penyakit)->pluck('id_gejala')->toArray();
        $arr_obat = BasisAturanObat::where('id_penyakit', $request->id_penyakit)->pluck('id_obat')->toArray();
        $penyakit = Penyakit::where('id',$request->id_penyakit)->first();

        $gejala = implode(',',$arr_gejala);
        $obat = implode(',',$arr_obat);
        $option = '<option value="'.$penyakit->id.'">'.$penyakit->penyakit.'</option>';

        $response = [
            'gejala'=>$gejala,
            'obat'=>$obat,
            'option'=>$option
        ];

        return response()->json($response); 

    }

    // function getDataObatEdit(Request $request){
    //     $arr_obat = BasisAturanObat::where('id_penyakit', $request->id_penyakit)->pluck('id_obat')->toArray();
    //     $penyakit = Penyakit::where('id',$request->id_penyakit)->first();

    //     $obat = implode(',',$arr_obat);
    //     $option = '<option value="'.$penyakit->id.'">'.$penyakit->penyakit.'</option>';

    //     $response = [
    //         'obat'=>$obat,
    //         'option'=>$option
    //     ];

    //     return response()->json($response); 

    // }

    function upload_foto($path, $file)
    {
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Paket_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path . '/' . $namaFile;
    }
}
