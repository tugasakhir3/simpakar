<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Gejala;
use App\Models\Kategori;
use App\Models\Paket;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class GejalaController extends Controller
{
    public function index()
    {
        $dt_user = DB::table('users as a')->leftJoin('alamatuser as b', 'a.id', '=', 'b.id_user')->leftJoin('ref_provinsi as c', 'b.province_id', '=', 'c.province_id')->leftJoin('ref_kab as d', 'b.city_id', '=', 'd.city_id')->leftJoin('ref_kec as e', 'b.subdistrict_id', '=', 'e.subdistrict_id')->select('a.*', 'c.province_name', 'd.city_name', 'e.subdistrict_name', 'b.detail_alamat')->where('role', 'admin')->whereNull('a.deleted_at')->get();

        $data = [
            'title' => 'Referensi Gejala',
            'menu' => 'Referensi Gejala',
            'li_active' => 'gejala',
            'dt_user' => $dt_user,
            'jml' => count($dt_user),
        ];
        return view('admin/gejala/index', $data);
    }

    public function dataGejala()
    {
        $query = Gejala::select('*'); // Select all columns from Paket model
        $data = $query->get();
        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    function hapus(Request $request)
    {
        $id = $request->id;
        $insert = Gejala::where('id', $id)->delete();
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => 'Berhasil Menghapus Gejala.',
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => 'Gagal Menghapus Gejala.',
            ];
        }

        echo json_encode($res);
    }

    // function tambah_admin(Request $request)
    // {
    //     $request->validate(
    //         [
    //             'username' => ['required', 'unique:' . User::class],
    //             'nama' => ['required'],
    //             'email' => ['required', 'email', 'unique:' . User::class],
    //             'password' => ['required'],
    //         ],
    //         [
    //             'username.required' => 'Username tidak boleh kosong.',
    //             'username.unique' => 'Username Sudah Digunakan.',
    //             'nama.required' => 'Nama tidak boleh kosong.',
    //             'email.required' => 'Email tidak boleh kosong.',
    //             'email.unique' => 'Email Sudah Digunakan.',
    //             'password.required' => 'Password tidak boleh kosong.',
    //         ],
    //     );

    //     $dt = [
    //         'username' => $request->username,
    //         'name' => $request->nama,
    //         'email' => $request->email,
    //         'password' => Hash::make($request->password),
    //         'reset_id' => $request->password,
    //         'role' => 'admin',
    //     ];

    //     $insert = User::insert($dt);

    //     if ($insert) {
    //         session()->flash('success', 'Admin berhasil di tambah.');
    //     } else {
    //         session()->flash('error', 'Admin gagal di tambah.');
    //     }
    //     return redirect()->route('daftar_admin');
    // }

    // public function tambah()
    // {
    //     $data = [
    //         'title' => 'Referensi Gejala',
    //         'menu' => 'Referensi',
    //         'li_active' => 'gejala',
    //     ];
    //     return view('admin/gejala/tambah', $data);
    // }

    // public function edit($id)
    // {
    //     $paket = Paket::where('id', $id)->first();
    //     $data = [
    //         'title' => 'Manajemen Paket',
    //         'menu' => 'Manajemen',
    //         'li_active' => 'paket',
    //         'paket' => $paket,
    //         'id' => $id,
    //     ];
    //     return view('admin/gejala/tambah', $data);
    // }

    // public function tambah_kaos()
    // {
    //     $data = [
    //         'page_title' => 'Tambah Kaos',
    //         'li_active' => 'produk',
    //         'li_sub_active' => 'kelola_kaos',
    //         'script_js' => 'admin/users/admin.js',
    //     ];
    //     return view('admin/users/adminah_kaos', $data);
    // }

    // public function tambah_produk()
    // {
    //     $ref_kategori = Kategori::all();
    //     $data = [
    //         'title' => 'Tambah Produk',
    //         'page_title' => 'Tambah Produk',
    //         'li_active' => 'produk',
    //         'li_sub_active' => 'kelola_kaos',
    //         'ref_kategori' => $ref_kategori,
    //         'script_js' => 'admin/users/adminuk.js',
    //     ];
    //     return view('admin/users/adminah_produk', $data);
    // }

    // public function edit_produk($id)
    // {
    //     $produk = t_produk::where('id', $id)->first();
    //     $ref_kategori = Kategori::all();
    //     $data = [
    //         'title' => 'Tambah Produk',
    //         'page_title' => 'Tambah Produk',
    //         'li_active' => 'produk',
    //         'li_sub_active' => 'kelola_kaos',
    //         'produk' => $produk,
    //         'ref_kategori' => $ref_kategori,
    //         'script_js' => 'admin/users/adminuk.js',
    //     ];
    //     return view('admin/users/adminah_produk', $data);
    // }

    function simpan(Request $request)
    {
        $id = $request->input('id');
        $request->validate(
            [
                'gejala' => 'required',
                'kode_gejala' => 'required',
            ],
            [
                'gejala.required' => 'Nama Gejala Tidak Boleh Kosong',
                'kode_gejala.required' => 'Nama Gejala Tidak Boleh Kosong',
            ],
        );

        $dt = [
            'gejala' => $request->gejala,
            'kode_gejala' => $request->kode_gejala,
        ];

        if ($id) {
            $dt['updated_at'] = now()->format('Y-m-d H:i:s');
            $insert = Gejala::where('id', $id)->update($dt);
            $pesan = 'di edit';
        } else {
            $dt['created_at'] = now()->format('Y-m-d H:i:s');
            $insert = Gejala::insert($dt);
            $pesan = 'di tambahkan';
        }

        if ($insert) {
            session()->flash('success', 'Gejala berhasil ' . $pesan . '.');
        } else {
            session()->flash('error', 'Gejala gagal ' . $pesan . '.');
        }

        return redirect()->route('gejala');
    }

    function upload_foto($path, $file)
    {
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Paket_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path . '/' . $namaFile;
    }
}
