<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\BasisAturan;
use App\Models\DetailKonsultasi;
use App\Models\Gejala;
use App\Models\Konsultasi;
use App\Models\Kategori;
use App\Models\Paket;
use App\Models\Penyakit;
use App\Models\t_produk;
use App\Models\t_transaksi;
use Carbon\Carbon;
use App\Models\User;
// use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

use PDF;

class KonsultasiController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Konsultasi',
            'menu' => 'Konsultasi',
            'li_active' => 'konsultasi',
        ];
        return view('admin/konsultasi/index', $data);
    }

    public function dataKonsultasi()
    {
        $query = Konsultasi::select('*')->with(['user','user_admin']); // Select all columns from Paket model
        if (Auth::user()->role == 'pasien') {
            $query->where('id_user', Auth::user()->id);
        }
        $data = $query->get();
        $data = $data->map(function ($item, $index) {
            return [
                'id' => $item->id,
                'encrypt_id' => Crypt::encrypt($item->id),
                'tanggal' => tanggal_indonesia($item->tanggal),
                'nama' => @$item->user->nama,
                'pertanyaan' => $item->pertanyaan,
                'jawaban' => $item->jawaban,
                'status' => $item->status,
                'admin' => @$item->user_admin->nama,
            ];
        });

        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    public function getDataKonsultasi(Request $request)
    {
        $arr_gejala = [];
        if ($request->id_konsultasi != null) {
            $arr_gejala = DetailKonsultasi::where('id_konsultasi', $request->id_konsultasi)
                ->pluck('id_gejala')
                ->toArray();
        }
        $data = Gejala::all();

        $data = $data->map(function ($item, $index) use ($arr_gejala) {
            $checked = in_array($item->id, $arr_gejala) ? 'checked' : '';
            return [
                'id' => $item->id,
                'gejala' => $item->gejala,
                'checked' => $checked,
            ];
        });

        return DataTables::of($data)->addIndexColumn()->make(true);
    }

    function getDataKonsultasiEdit(Request $request)
    {
        $arr_gejala = DetailKonsultasi::where('id_konsultasi', $request->id)
            ->pluck('id_gejala')
            ->toArray();

        $gejala = implode(',', $arr_gejala);

        $response = [
            'gejala' => $gejala,
        ];

        return response()->json($response);
    }

    function hapus(Request $request)
    {
        $id = $request->id;
        $insert = Konsultasi::where('id', $id)->delete();
        if ($insert) {
            $res = [
                'status' => true,
                'pesan' => 'Berhasil Menghapus Konsultasi.',
            ];
        } else {
            $res = [
                'status' => false,
                'pesan' => 'Gagal Menghapus Konsultasi.',
            ];
        }

        echo json_encode($res);
    }

    public function tambah_konsultasi()
    {
        $gejala = Gejala::get();
        $data = [
            'title' => 'Tambah Konsultasi',
            'menu' => 'Konsultasi',
            'li_active' => 'Tambah konsultasi',
            'gejala' => $gejala
        ];
        return view('admin/konsultasi/tambah_konsultasi', $data);
    }

    public function edit_konsultasi($id)
    {
        $gejala = Gejala::get();
        $arr_gejala = DetailKonsultasi::where('id_konsultasi', Crypt::decrypt($id))
            ->pluck('id_gejala')
            ->toArray();
        $jawaban = implode(',', $arr_gejala);
        $data = [
            'title' => 'Tambah Konsultasi',
            'menu' => 'Konsultasi',
            'li_active' => 'Tambah konsultasi',
            'gejala' => $gejala,
            'jawaban' => $jawaban,
            'arr_gejala' => $arr_gejala,
            'id' =>  Crypt::decrypt($id)
        ];
        return view('admin/konsultasi/edit_konsultasi', $data);
    }

    function detail_konsultasi($id)
    {
        $dt = Konsultasi::with(['detail', 'user'])
            ->where('id', $id)
            ->first();

        $role = Auth::user()->role;

        $hasil_penyakit = $data = DB::table('ref_penyakit as a')
            ->select('a.*', DB::raw('COUNT(b.id_gejala) as gejala'), DB::raw('(SELECT COUNT(id_gejala) FROM basis_aturan WHERE id_penyakit = a.id) as jml_gejala'), DB::raw('(COUNT(b.id_gejala) / (SELECT COUNT(id_gejala) FROM basis_aturan WHERE id_penyakit = a.id) * 100) as persentase'))
            ->leftJoin('basis_aturan as b', 'a.id', '=', 'b.id_penyakit')
            ->leftJoin('ref_gejala as c', 'b.id_gejala', '=', 'c.id')
            ->whereIn('b.id_gejala', function ($query) use ($id) {
                $query->select('id_gejala')->from('detail_konsultasi')->where('id_konsultasi', $id);
            })
            ->groupBy('a.id', 'a.penyakit')
            ->get();

            foreach ($hasil_penyakit as $key) {
                $key->obat = DB::select(
                    'SELECT a.*, c.obat
                     FROM basis_aturan_obat a
                     JOIN ref_penyakit b ON a.id_penyakit = b.id
                     JOIN ref_obat c ON a.id_obat = c.id
                     WHERE a.id_penyakit = ?',
                    [$key->id],
                );
            }

        $data = [
            'title' => 'Detail Konsultasi',
            'menu' => 'Detail Konsultasi',
            'li_active' => 'konsultasi',
            'hasil' => $hasil_penyakit,
            'dt' => $dt,
            'role'=> $role
        ];

        return view('admin/konsultasi/detail', $data);
    }

    function cetak_konsultasi($id)
    {
        $dt = Konsultasi::with(['detail', 'user'])
            ->where('id', $id)
            ->first();

        $hasil_penyakit = DB::table('ref_penyakit as a')
            ->select('a.*', DB::raw('COUNT(b.id_gejala) as gejala'), DB::raw('(SELECT COUNT(id_gejala) FROM basis_aturan WHERE id_penyakit = a.id) as jml_gejala'), DB::raw('(COUNT(b.id_gejala) / (SELECT COUNT(id_gejala) FROM basis_aturan WHERE id_penyakit = a.id) * 100) as persentase'))
            ->leftJoin('basis_aturan as b', 'a.id', '=', 'b.id_penyakit')
            ->leftJoin('ref_gejala as c', 'b.id_gejala', '=', 'c.id')
            ->whereIn('b.id_gejala', function ($query) use ($id) {
                $query->select('id_gejala')->from('detail_konsultasi')->where('id_konsultasi', $id);
            })
            ->groupBy('a.id', 'a.penyakit')
            ->get();

            foreach ($hasil_penyakit as $key) {
                $key->obat = DB::select(
                    'SELECT a.*, c.obat
                     FROM basis_aturan_obat a
                     JOIN ref_penyakit b ON a.id_penyakit = b.id
                     JOIN ref_obat c ON a.id_obat = c.id
                     WHERE a.id_penyakit = ?',
                    [$key->id],
                );
            }

        $data = [
            'hasil' => $hasil_penyakit,
            'dt' => $dt,
        ];

        $pdf = Pdf::loadView('admin.konsultasi.cetak', $data);
        return $pdf->stream('konsultasi.pdf');
    }

    function simpan(Request $request)
    {
        $id = $request->input('id');
        $arr_gejala = $request->input('arr_gejala');

        if ($arr_gejala == '[]' || $arr_gejala == '') {
            session()->flash('error', 'Wajib menambahkan gejala.');
        } else {
            if (empty($id)) {
                $insert = [
                    'id_user' => Auth::user()->id,
                    'tanggal' => date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                $konsultasi = Konsultasi::create($insert);
                $id = $konsultasi->id;
            } else {
                $id = $id;
            }

            $dt = [];
            $array = explode(',', $arr_gejala);
            $jml_arr = count($array);

            for ($i = 0; $i < $jml_arr; $i++) {
                $dt[] = [
                    'id_konsultasi' => $id,
                    'id_gejala' => $array[$i],
                    'created_at' => now()->format('Y-m-d H:i:s'),
                ];
            }

            DetailKonsultasi::where('id_konsultasi', $id)->delete();
            $insert = DetailKonsultasi::insert($dt);
            $pesan = 'di tambahkan';

            if ($insert) {
                session()->flash('success', 'Konsultasi berhasil ' . $pesan . '.');
            } else {
                session()->flash('error', 'Konsultasi gagal ' . $pesan . '.');
            }
        }

        return redirect()->route('konsultasi');
    }

    function simpan_feedback(Request $request)
    {
        $id = $request->id;
        $pertanyaan = $request->pertanyaan;
        $jawaban = $request->jawaban;

        $dt = [
            'pertanyaan' => $pertanyaan,
            'jawaban' => $jawaban,
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        if (Auth::user()->role == 'admin') {
            $dt['is_admin'] = Auth::user()->id;
        }

        $insert = Konsultasi::where('id', $id)->update($dt);

        if ($insert) {
            session()->flash('success', 'Berhasil di Simpan.');
        } else {
            session()->flash('error', 'Gagal di Simpan.');
        }

        return redirect('detail_konsultasi/'.$id);
    }
    
    function selesai_feedback(Request $request)
    {
        $id = $request->id;
        // return $request->all();

        $dt = [
            'updated_at' => date('Y-m-d H:i:s'),
            'status' => '1'
        ];
        
        $insert = Konsultasi::where('id', $id)->update($dt);

        if ($insert) {
            $response = [
                'status'=>true,
                'pesan'=> 'Berhasil Menyimpan.'
            ];
        } else {
            $response = [
                'status'=>false,
                'pesan'=> 'Gagal Menyimpan.'
            ];
        }

        return response()->json($response);
    }

    function upload_foto($path, $file)
    {
        if (!is_dir(storage_path("app/public/{$path}"))) {
            mkdir(storage_path("app/public/{$path}"), 0777, true);
        }

        $nama = $file->getClientOriginalName();
        $ext = pathinfo($nama, PATHINFO_EXTENSION);
        $namaFile = 'Paket_' . time() . '.' . $ext;
        $file->storeAs("public/{$path}", $namaFile);

        return $path . '/' . $namaFile;
    }
}
