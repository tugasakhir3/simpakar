<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\t_produk;
use App\Models\Ukuran;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DetailprodukController extends Controller
{
    function detail_kaos($id){
        $ref_ukuran = Ukuran::all();
        $dt_produk = t_produk::where('id',$id)->first();
        $data = [
            'li_active' =>"tshirt",
            'ref_ukuran' => $ref_ukuran,
            'dt_produk' => $dt_produk,
            'script_js' => 'customer/detail_kaos.js'
        ];
        return view('front/produk/detail_kaos',$data);
    }
    
    function detail_tas($id){
        $dt_produk = t_produk::where('id',$id)->first();
        $data = [
            'li_active' =>"tas",
            'dt_produk' => $dt_produk,
            'script_js' => 'customer/detail_tas.js'
        ];
        return view('front/produk/detail_tas',$data);
    }
}
