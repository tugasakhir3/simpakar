<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Models\t_paket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class FrontController extends Controller
{
    function index()
    {
        $data = [
            'li_active' => 'home',
        ];
        return view('front/home', $data);
    }

    // function cek()
    // {
    //     $date = '2023-01-01 11:44:45';
    //     $data = [
    //         'cek' => Carbon::parse($date)->isoFormat('dddd, D MMMM Y'),
    //     ];
    //     return view('front/cek', $data);
    // }

    // function search_produk(Request $request)
    // {
    //     $keyword = $request->keyword;
    //     $result = [];
    //     if ($keyword != '' || $keyword != null) {
    //         $result = t_produk::whereNull('deleted_at')
    //             ->where('nama_produk', 'like', '%' . $keyword . '%')
    //             ->limit(5)
    //             ->get();
    //     }
    //     $data = [
    //         'data' => $result,
    //         'keyword' => $keyword,
    //     ];
    //     $view = view('front/result_search', $data);
    //     return $view;
    // }

    // function result_produk($keyword)
    // {
    //     $result = t_produk::whereNull('deleted_at')
    //         ->where('nama_produk', 'like', '%' . $keyword . '%')
    //         ->get();
    //     $data = [
    //         'li_active' => 'search',
    //         'data' => $result,
    //     ];

    //     return view('front/search', $data);
    // }
}
