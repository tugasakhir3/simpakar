<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create(): View
    {
        $data = [
            "js" => @include('login.js')
        ];
        return view('login.index', $data);
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request)
    {
        $input = $request->all();
        // return $input; 
        $this->validate($request, [
            // 'username' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        // if(auth()->attempt(array('username' => $input['username'], 'password' => $input['password']))){
        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))){
            if (auth()->user()->role == 'admin') {
                return redirect()->route('dashboard_admin');
            }else{
                return redirect()->route('dashboard_user');
                // return redirect()->route('home');
            }
        }else{
            return redirect()->route('login')->with('error','Email dan Password Salah.');
        }
        // $request->authenticate();

        // $request->session()->regenerate();

        // return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
