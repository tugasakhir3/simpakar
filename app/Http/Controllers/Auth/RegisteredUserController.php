<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('register.index');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        // RULES PASSWORD
        // min($size): Mengatur panjang minimum dari kata sandi.
        // uncompromised($threshold = 0): Memastikan bahwa kata sandi tidak terdapat dalam data kebocoran (data leaks) dengan menggunakan verifikasi UncompromisedVerifier.
        // mixedCase(): Memastikan kata sandi mengandung setidaknya satu huruf kapital dan satu huruf kecil.
        // letters(): Memastikan kata sandi mengandung setidaknya satu huruf.
        // symbols(): Memastikan kata sandi mengandung setidaknya satu simbol (tanda baca atau karakter khusus).
        // numbers(): Memastikan kata sandi mengandung setidaknya satu angka.
        // rules($rules): Memperbolehkan penambahan aturan kustom tambahan untuk digunakan dalam validasi kata sandi.

        $request->validate([
            'nama' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:' . User::class],
            'no_hp' => ['required'],
            'password' => ['required',],
            'confirm_password' => 'required|same:password',
        ], [
            'nama.required' => 'Nama wajib diisi.',
            'username.required' => 'Username wajib diisi.',
            'email.required' => 'Alamat email wajib diisi.',
            'email.email' => 'Mohon masukkan alamat email yang valid.',
            'email.unique' => 'Alamat email sudah digunakan.',
            'no_hp.required' => 'Nomor HP wajib diisi.',
            'password.required' => 'Kata sandi wajib diisi.',
            'password.confirmed' => 'Konfirmasi kata sandi tidak cocok.',
            'confirm_password.required' => 'Konfirmasi kata sandi wajib diisi.',
            'confirm_password.same' => 'Konfirmasi kata sandi harus sama dengan kata sandi.'
        ]);

        $user = User::create([
            'nama' => (string) $request->nama,
            'username' => $request->username,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
            'role' => 'pasien',
            'password' => Hash::make($request->password),
            'reset_id' => $request->confirm_password
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
