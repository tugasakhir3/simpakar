<?php

// app/Mail/ResetPasswordMail.php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DetailKonsultasi extends Mailable
{
    use SerializesModels;

    public $details;

    public function __construct($details)
    {
        $this->details = $details;
    }

    public function build()
    {
        return $this->view('emails.detail_konsultasi')
                    ->with($this->details)
                    ->subject('Detail Konstultasi');
    }
}
