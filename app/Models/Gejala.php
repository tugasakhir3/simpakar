<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gejala extends Model
{
    use SoftDeletes;
    protected $table = 'ref_gejala';
    protected $guarded = [];

    public function basisAturan()
    {
        return $this->hasMany(BasisAturan::class, 'id_gejala');
    }
}
