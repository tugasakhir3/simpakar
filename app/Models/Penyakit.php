<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Penyakit extends Model
{
    use SoftDeletes;
    protected $table = 'ref_penyakit';
    protected $guarded = [];

    // function RefBasis(){
    //     return $this->belongsTo(BasisAturan::class, 'id_penyakit');
    // }
    public function basisAturan()
    {
        return $this->hasMany(BasisAturan::class, 'id_penyakit');
    }
}
