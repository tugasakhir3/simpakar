<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Obat extends Model
{
    use SoftDeletes;
    protected $table = 'ref_obat';
    protected $guarded = [];

    public function basisAturan()
    {
        return $this->hasMany(BasisAturanObat::class, 'id_obat');
    }
}
