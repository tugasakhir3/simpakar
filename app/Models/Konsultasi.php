<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Konsultasi extends Model
{
    protected $table = 'konsultasi';
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne(User::class, 'id','id_user');
    }

    public function user_admin()
    {
        return $this->hasOne(User::class, 'id','is_admin');
    }

    public function detail()
    {
        return $this->hasMany(DetailKonsultasi::class, 'id_konsultasi','id')->with(['gejala']);
    }
}
