<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailKonsultasi extends Model
{
    protected $table = 'detail_konsultasi';
    protected $guarded = [];

    public function gejala()
    {
        return $this->hasOne(Gejala::class, 'id','id_gejala');
    }
}
