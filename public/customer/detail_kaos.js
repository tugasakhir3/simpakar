$(document).ready(function(){
    $("#tambah_keranjang").click(function(){
        $("#tambah_keranjang").prop('disable',true);
        let id_produk = $(this).data('id_produk');
        let jumlah = $("#jumlah").val();
        let ukuran = $("#ukuran").val();
        let csrfToken = $('meta[name="csrf-token"]').attr('content');
        $("#tambah_keranjang").prop('disabled',true);
        Swal.fire({
            text: "Anda yakin akan menambahkan ke keranjang?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Tambah'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/simpan_keranjang',
                    type:'post',
                    data:{
                        id_produk:id_produk,
                        jumlah:jumlah,
                        ukuran:ukuran,
                        _token: csrfToken
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Berhasil!',
                                res.pesan,
                                'success'
                                )
                            load_cart();
                        }else{
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                                )
                        }
                        // setTimeout(() => {
                        //     window.location.reload(true);
                        // }, 1000);
                    }
                })
            }
          })
          $("#tambah_keranjang").prop('disabled',false);
    })
})