$(document).ready(function() {
  $("#provinsi").change(function(){
    $("#kabupaten").html("<option>Loading</option>")
    $("#kecamatan").html("<option>Loading</option>")
      $.ajax({
        url: '/get_kabupaten',
        type: 'get',
        dataType: 'json',
        data: {
        province_id: $("#provinsi").val()
        },
        success: function(res) {
            console.log(res);
          $("#kabupaten").empty();
          let kabupaten = '<option value="">-- Pilih Kabupaten --</option>';
          if (res.length > 0) {
            $.each(res, function(index, kab) {
              kabupaten += '<option value="' + kab.city_id + '">' + kab.city_name + '</option>';
            });
            $("#kabupaten").append(kabupaten);
            $("#kecamatan").html("<option>-- Pilih Kecamatan --</option>")
        } else {
            $("#kabupaten").append('<option value="">Tidak ada kabupaten ditemukan</option>');
            $("#kecamatan").html("<option>-- Pilih Kecamatan --</option>")
          }
        }
    })
  })

  $("#kabupaten").change(function(){
    $("#kecamatan").html("<option>Loading</option>")
      $.ajax({
        url: '/get_kecamatan',
        type: 'get',
        dataType: 'json',
        data: {
        city_id: $("#kabupaten").val()
        },
        success: function(res) {
          $("#kecamatan").empty();
          let kecamatan = '<option value="">-- Pilih kecamatan --</option>';
          if (res.length > 0) {
            $.each(res, function(index, kec) {
              kecamatan += '<option value="' + kec.subdistrict_id + '">' + kec.subdistrict_name + '</option>';
            });
            $("#kecamatan").append(kecamatan);
          } else {
            $("#kecamatan").append('<option value="">Tidak ada kecamatan ditemukan</option>');
          }
        }
    })
  })
})
