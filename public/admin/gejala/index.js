$(document).ready(function() {
    load_table();

    $("#tambahGejala").on('click',function(){
        $("#formGejala").trigger('reset');
        $("#ModaltambahGejala").modal('show');
    })
})

function load_table(){
    table = $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: dataGejala,
            type: 'get',
            dataType: 'json'
        },
        order: [[3, 'desc']],
        columns: [{
            data: 'DT_RowIndex'
        },
        {
            data: 'kode_gejala',
        },
        {
            data: 'gejala',
        },
        {
            data: '',
            render: (data, type, row) => {
                let button = '';
                button += `<div class="mb-1 text-center">
                    <button class="btn btn-danger btn-hapus" data-id="${row.id}"><i class="bx bx-trash"></i></button>
                    <button class="btn btn-warning btn-edit" data-id="${row.id}" data-kode_gejala="${row.kode_gejala}" data-gejala="${row.gejala}"><i class="bx bx-edit"></i></button>
                </div>`;
                return button;
            }
        },
        ]
    });
}

$(document).ready(function(){
    $("#dataTable").on('click', '.btn-hapus', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        let id = $(this).data('id');

        Swal.fire({
            text: "Anda akan menghapus data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: HapusGejala,
                    type:'post',
                    data:{
                        id:id,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terhapus!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        table.ajax.reload();
                    }
                })
            }
          })
    });

    $("#dataTable").on('click', '.btn-edit', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        $("#formGejala").trigger('reset');
        let id = $(this).data('id');
        let gejala = $(this).data('gejala');
        let kode_gejala = $(this).data('kode_gejala');
        
        $('input[name="id"]').val(id);
        $('input[name="gejala"]').val(gejala);
        $('input[name="kode_gejala"]').val(kode_gejala);
        $("#ModaltambahGejala").modal('show');

        // Swal.fire({
        //     text: "Anda akan menghapus data ini, anda yakin?",
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     cancelButtonText: 'Batal',
        //     confirmButtonText: 'Ya, Hapus'
        //   }).then((result) => {
        //     if (result.isConfirmed) {
        //         $.ajax({
        //             url:HapusGejala,
        //             type:'post',
        //             data:{
        //                 id:id,
        //                 _token: token
        //             },
        //             dataType:'json',
        //             success:function(res){
        //                 if (res) {
        //                     Swal.fire(
        //                         'Terhapus!',
        //                         res.pesan,
        //                         'success'
        //                     )
        //                 } else {
        //                     Swal.fire(
        //                         'Gagal!',
        //                         res.pesan,
        //                         'error'
        //                     )
        //                 }
        //                 table.ajax.reload();
        //             }
        //         })
        //     }
        //   })
    });
});


// function hapus(id){
//   let csrfToken = $('meta[name="csrf-token"]').attr('content');
//   Swal.fire({
//       text: "Anda yakin ingin menghapus User ini?",
//       icon: 'warning',
//       showCancelButton: true,
//       confirmButtonColor: '#3085d6',
//       cancelButtonColor: '#d33',
//       cancelButtonText: 'Batal',
//       confirmButtonText: 'Ya, Hapus'
//     }).then((result) => {
//       if (result.isConfirmed) {
//           $.ajax({
//               url: '/admin/hapus_admin',
//               type:'post',
//               data:{
//                   id:id,
//                   _token: csrfToken
//               },
//               dataType:'json',
//               success:function(res){
//                   if (res) {
//                       Swal.fire(
//                           'Berhasil!',
//                           res.pesan,
//                           'success'
//                           )
//                         // load_table();
//                       setTimeout(function() {
//                           location.reload(true); // true mengabaikan cache dan memaksa memuat ulang dari server
//                       }, 1000);
//                   }else{
//                       Swal.fire(
//                           'Gagal!',
//                           res.pesan,
//                           'error'
//                           )
//                   }
//               }
//           })
//       }
//     })
// }
