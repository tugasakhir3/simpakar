$(document).ready(function() {
    load_table();

    $("#tambah").on('click',function(){
        $("#form").trigger('reset');
        $("#Modaltambah").modal('show');
        load_basis_aturan();
    })


})

function load_basis_aturan(){
    $('#dataTableGejala').DataTable().destroy()
    $('#dataTableGejala').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataGejala,
                type: 'get',
                dataType: 'json'
            },
            columns: [{
                data: 'DT_RowIndex'
            },
            {
                data: 'gejala',
            },
            {
                data: '',
                render: (data, type, row) => {
                    let result = `<div class="form-check text-center">
                        <input class="form-check-input form-control" type="checkbox" onclick="checklist(this)" value="${row.id}" id="flexCheckChecked" >
                    </div>`;

                    return result;
                }
            },
        ]
    });
    $('#dataTableObat').DataTable().destroy()
    $('#dataTableObat').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataObat,
                type: 'get',
                dataType: 'json'
            },
            columns: [{
                data: 'DT_RowIndex'
            },
            {
                data: 'obat',
            },
            {
                data: '',
                render: (data, type, row) => {
                    let result = `<div class="form-check text-center">
                        <input class="form-check-input form-control" type="checkbox" onclick="checklist_obat(this)" value="${row.id}" id="flexCheckChecked" >
                    </div>`;

                    return result;
                }
            },
        ]
    });
}

function load_basis_aturan_edit(id_penyakit){
    $('#dataTableGejalaEdit').DataTable().destroy()
    $('#dataTableGejalaEdit').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataGejala,
                type: 'get',
                dataType: 'json',
                data:{
                    id_penyakit:id_penyakit
                }
            },
            columns: [{
                data: 'DT_RowIndex'
            },
            {
                data: 'gejala',
            },
            {
                data: '',
                render: (data, type, row) => {
                    let result = `<div class="form-check text-center">
                        <input class="form-check-input form-control" type="checkbox" onclick="checklist_edit(this)" value="${row.id}" ${row.checked} id="flexCheckChecked" >
                    </div>`;

                    return result;
                }
            },
        ]
    });
    $('#dataTableObatEdit').DataTable().destroy()
    $('#dataTableObatEdit').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataObat,
                type: 'get',
                dataType: 'json',
                data:{
                    id_penyakit:id_penyakit
                }
            },
            columns: [{
                data: 'DT_RowIndex'
            },
            {
                data: 'obat',
            },
            {
                data: '',
                render: (data, type, row) => {
                    let result = `<div class="form-check text-center">
                        <input class="form-check-input form-control" type="checkbox" onclick="checklist_edit_obat(this)" value="${row.id}" ${row.checked} id="flexCheckChecked" >
                    </div>`;

                    return result;
                }
            },
        ]
    });
}

function load_table(){
    table = $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: dataBasisAturan,
            type: 'get',
            dataType: 'json'
        },
        order: [[2, 'desc']],
        columns: [{
            data: 'DT_RowIndex'
        },
        {
            data: 'penyakit',
        },
        {
            data: 'keterangan',
        },
        {
            data: '',
            render: (data, type, row) => {
                array = row.gejala;
                let result = '';
                // let result = '<div class="mb-1" style="font-size:12px;">';
                array.forEach(element => {
                    result += '- ' + element.gejala + '<br><br>';
                });
                result += '</div>';

                return result;
            }
        },
        {
            data: '',
            render: (data, type, row) => {
                array = row.obat;
                let result = '';
                array.forEach(element => {
                    result += '- ' + element.obat + '<br><br>';
                });
                result += '</div>';

                return result;
            }
        },
        {
            data: '',
            render: (data, type, row) => {
                let button = '';
                button += `<div class="mb-1 text-center">
                    <button class="btn btn-danger btn-hapus" data-id_penyakit="${row.id_penyakit}"><i class="bx bx-trash"></i></button>
                    <button class="btn btn-warning btn-edit" data-id_penyakit="${row.id_penyakit}" data-penyakit="${row.penyakit}"><i class="bx bx-edit"></i></button>
                </div>`;
                return button;
            }
        },
        ]
    });
}

function checklist(checkbox) {
    const id = $(checkbox).val();
    let idsArray = selectedIds ? selectedIds.split(',') : [];

    if ($(checkbox).is(':checked')) {
        if (!idsArray.includes(id)) {
            idsArray.push(id);
        }
    } else {
        const index = idsArray.indexOf(id);
        if (index > -1) {
            idsArray.splice(index, 1);
        }
    }

    selectedIds = idsArray.join(',');
    $('#Modaltambah #arrGejalaInput').val(selectedIds);
}

function checklist_obat(checkbox) {
    const id = $(checkbox).val();
    let idsArray = selectedIdsObat ? selectedIdsObat.split(',') : [];

    if ($(checkbox).is(':checked')) {
        if (!idsArray.includes(id)) {
            idsArray.push(id);
        }
    } else {
        const index = idsArray.indexOf(id);
        if (index > -1) {
            idsArray.splice(index, 1);
        }
    }

    selectedIdsObat = idsArray.join(',');
    $('#Modaltambah #arrObatInput').val(selectedIdsObat);
}

function checklist_edit(checkbox) {
    const id = $(checkbox).val();

    let selectedIds = $('#Modaledit #arrGejalaInput').val();
    let idsArray = selectedIds ? selectedIds.split(',') : [];

    if ($(checkbox).is(':checked')) {
        if (!idsArray.includes(id)) {
            idsArray.push(id);
        }
    } else {
        const index = idsArray.indexOf(id);
        if (index > -1) {
            idsArray.splice(index, 1);
        }
    }

    selectedIds = idsArray.join(',');
    $('#Modaledit #arrGejalaInput').val(selectedIds);
}

function checklist_edit_obat(checkbox) {
    const id = $(checkbox).val();

    let selectedIdsObat = $('#Modaledit #arrObatInput').val();
    let idsArray = selectedIdsObat ? selectedIdsObat.split(',') : [];

    if ($(checkbox).is(':checked')) {
        if (!idsArray.includes(id)) {
            idsArray.push(id);
        }
    } else {
        const index = idsArray.indexOf(id);
        if (index > -1) {
            idsArray.splice(index, 1);
        }
    }

    selectedIdsObat = idsArray.join(',');
    $('#Modaledit #arrObatInput').val(selectedIdsObat);
}

$(document).ready(function(){
    $("#dataTable").on('click', '.btn-hapus', function(e){
        e.preventDefault(); 
        let id_penyakit = $(this).data('id_penyakit');

        Swal.fire({
            text: "Anda akan menghapus data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url:HapusBasisAturan,
                    type:'post',
                    data:{
                        id_penyakit:id_penyakit,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terhapus!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        table.ajax.reload();
                    }
                })
            }
          })
    });

    $("#dataTable").on('click', '.btn-edit', function(e){
        e.preventDefault(); 
        let id_penyakit = $(this).data('id_penyakit');
        load_basis_aturan_edit(id_penyakit);
        $("#Modaledit #form").trigger('reset');
        $("#Modaledit").modal('show');
        $("#Modaledit .loading").show();
        $("#Modaledit .form-edit").hide();
        
        $.ajax({
            url: getDataGejalaEdit,
            type:'get',
            dataType:'json',
            data:{
                id_penyakit:id_penyakit
            },
            success:function(res){
                $("#Modaledit #penyakit").html(res.option);
                $("#Modaledit #arrGejalaInput").val(res.gejala);
                $("#Modaledit #arrObatInput").val(res.obat);
                $("#Modaledit .loading").hide();
                $("#Modaledit .form-edit").show();
            }
        })
        
    });
    
});


