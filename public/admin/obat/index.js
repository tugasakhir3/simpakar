$(document).ready(function() {
    load_table();

    $("#tambahObat").on('click',function(){
        $("#formObat").trigger('reset');
        $("#ModaltambahObat").modal('show');
    })
})

function load_table(){
    table = $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: dataObat,
            type: 'get',
            dataType: 'json'
        },
        order: [[2, 'desc']],
        columns: [{
            data: 'DT_RowIndex'
        },
        {
            data: 'obat',
        },
        {
            data: 'keterangan',
        },
        {
            data: '',
            render: (data, type, row) => {
                let button = '';
                button += `<div class="mb-1 text-center">
                    <button class="btn btn-danger btn-hapus" data-id="${row.id}"><i class="bx bx-trash"></i></button>
                    <button class="btn btn-warning btn-edit" data-id="${row.id}" data-keterangan="${row.keterangan}" data-obat="${row.obat}"><i class="bx bx-edit"></i></button>
                </div>`;
                return button;
            }
        },
        ]
    });
}

$(document).ready(function(){
    $("#dataTable").on('click', '.btn-hapus', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        let id = $(this).data('id');

        Swal.fire({
            text: "Anda akan menghapus data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: HapusObat,
                    type:'post',
                    data:{
                        id:id,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terhapus!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        table.ajax.reload();
                    }
                })
            }
          })
    });

    $("#dataTable").on('click', '.btn-edit', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        $("#formObat").trigger('reset');
        let id = $(this).data('id');
        let obat = $(this).data('obat');
        let keterangan = $(this).data('keterangan');
        
        $('input[name="id"]').val(id);
        $('input[name="obat"]').val(obat);
        $('textarea[name="keterangan"]').val(keterangan);
        $("#ModaltambahObat").modal('show');

    });
});


