$(document).ready(function() {
    load_table();

    $("#tambahGejala").on('click',function(){
        $("#formGejala").trigger('reset');
        $("#ModaltambahGejala").modal('show');
    })
})

function load_table(){
    table = $('#dataTable').DataTable();
}

$(document).ready(function(){
    $("#dataTable").on('click', '.btn-hapus', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        let id = $(this).data('id');

        Swal.fire({
            text: "Anda akan menghapus data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: HapusGejala,
                    type:'post',
                    data:{
                        id:id,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terhapus!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        table.ajax.reload();
                    }
                })
            }
          })
    });

    $("#dataTable").on('click', '.btn-edit', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        $("#formGejala").trigger('reset');
        let id = $(this).data('id');
        let gejala = $(this).data('gejala');
        
        $('input[name="id"]').val(id);
        $('input[name="gejala"]').val(gejala);
        $("#ModaltambahGejala").modal('show');

        // Swal.fire({
        //     text: "Anda akan menghapus data ini, anda yakin?",
        //     icon: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     cancelButtonText: 'Batal',
        //     confirmButtonText: 'Ya, Hapus'
        //   }).then((result) => {
        //     if (result.isConfirmed) {
        //         $.ajax({
        //             url:HapusGejala,
        //             type:'post',
        //             data:{
        //                 id:id,
        //                 _token: token
        //             },
        //             dataType:'json',
        //             success:function(res){
        //                 if (res) {
        //                     Swal.fire(
        //                         'Terhapus!',
        //                         res.pesan,
        //                         'success'
        //                     )
        //                 } else {
        //                     Swal.fire(
        //                         'Gagal!',
        //                         res.pesan,
        //                         'error'
        //                     )
        //                 }
        //                 table.ajax.reload();
        //             }
        //         })
        //     }
        //   })
    });
});

function selesai(id){
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    Swal.fire({
        text: "Anda yakin ingin Menyelesaikan Konsultasi ini?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batal',
        confirmButtonText: 'Ya, Selesaikan'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: selesaiFeedback,
                type:'post',
                data:{
                    id:id,
                    _token: csrfToken
                },
                dataType:'json',
                success:function(res){
                    if (res) {
                        Swal.fire(
                            'Berhasil!',
                            res.pesan,
                            'success'
                            )
                        setTimeout(function() {
                            location.reload(true); // true mengabaikan cache dan memaksa memuat ulang dari server
                        }, 1000);
                    }else{
                        Swal.fire(
                            'Gagal!',
                            res.pesan,
                            'error'
                            )
                    }
                }
            })
        }
      })
  }
