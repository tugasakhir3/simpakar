$(document).ready(function() {
    load_table();

    $("#tambahKonsultasi").on('click',function(){
        load_konsultasi();
        $("#formKonsultasi").trigger('reset');
        $("#ModaltambahKonsultasi").modal('show');
    })
})

function load_konsultasi(){
    $('.loading').show();
    $('.form-konsultasi').hide();
    $('#dataTableKonsultasi').DataTable().destroy()
    $('#dataTableKonsultasi').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: dataGejala,
                type: 'get',
                dataType: 'json'
            },
            columns: [{
                data: 'DT_RowIndex'
            },
            {
                data: 'gejala',
            },
            {
                data: '',
                render: (data, type, row) => {
                    let result = `<div class="form-check text-center">
                        <input class="form-check-input form-control" type="checkbox" onclick="checklist(this)" value="${row.id}" id="flexCheckChecked" >
                    </div>`;

                    return result;
                }
            },
        ]
    });

    $('.loading').hide();
    $('.form-konsultasi').show();
}

function load_konsultasi_edit(id_konsultasi){
    $('#dataTableKonsultasi').DataTable().destroy()
    $('#dataTableKonsultasi').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: getDataKonsultasi,
                type: 'get',
                dataType: 'json',
                data:{
                    id_konsultasi:id_konsultasi
                }
            },
            columns: [{
                data: 'DT_RowIndex'
            },
            {
                data: 'gejala',
            },
            {
                data: '',
                render: (data, type, row) => {
                    let result = `<div class="form-check text-center">
                        <input class="form-check-input form-control" type="checkbox" onclick="checklist_edit(this)" value="${row.id}" ${row.checked} id="flexCheckChecked" >
                    </div>`;

                    return result;
                }
            },
        ]
    });
}

function load_table(){
    table = $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: dataKonsultasi,
            type: 'get',
            dataType: 'json'
        },
        order: [[2, 'desc']],
        columns: [{
            data: 'DT_RowIndex'
        },
        {
            data: 'tanggal',
        },
        {
            data: 'nama',
        },
        {
            data: '',
            render:(data,type, row)=>{
                let result = '<div class="text-center">';
                if (row.status == '1') {
                    result += '<h6><span class="badge badge-sm bg-primary">Sudah Selesai</span></h6>'
                } else {
                    if (row.pertanyaan != null && row.jawaban == null) {
                        result += '<h6><span class="badge badge-sm bg-warning">Pertanyaan Belum di Jawab</span></h6>'
                    } else if(row.pertanyaan != null && row.jawaban != null) {
                        result += '<h6><span class="badge badge-sm bg-success">Pertanyaan Sudah di Jawab</span></h6>'
                    } else {
                        result += '<h6><span class="badge badge-sm bg-danger">Belum Selesai</span></h6>'
                    }
                }

                result += '</div>'
                return result;
            }
        },
        {
            data: '',
            render: (data, type, row) => {
                let button = '';
                if (role == 'admin') {
                    button += `<div class="mb-1 text-center">
                        <a href="${detailKonsultasi}/${row.id}" class="btn btn-primary btn-detail" data-id="${row.id}"><i class="bx bx-list-ul"></i></a>
                        <a href="${detailKonsultasi}/${row.id}" class="btn btn-warning btn-send" alt="Kirim ke Gmail" data-id="${row.id}"><i class="bx bx-send"></i></a>
                    </div>`;
                }else{
                    button += `<div class="mb-1 text-center">
                        <a href="${detailKonsultasi}/${row.id}" class="btn btn-primary btn-detail" data-id="${row.id}"><i class="bx bx-list-ul"></i></a>
                        <button class="btn btn-danger btn-hapus" data-id="${row.id}"><i class="bx bx-trash"></i></button>
                        <a href="edit_konsultasi/${row.encrypt_id}" class="btn btn-warning" data-id="${row.id}" data-gejala="${row.gejala}"><i class="bx bx-edit"></i></a>
                    </div>`;
                    // button += `<div class="mb-1 text-center">
                    //     <a href="${detailKonsultasi}/${row.id}" class="btn btn-primary btn-detail" data-id="${row.id}"><i class="bx bx-list-ul"></i></a>
                    //     <button class="btn btn-danger btn-hapus" data-id="${row.id}"><i class="bx bx-trash"></i></button>
                    //     <button class="btn btn-warning btn-edit" data-id="${row.id}" data-gejala="${row.gejala}"><i class="bx bx-edit"></i></button>
                    // </div>`;
                }
                return button;
            }
        },
        ]
    });
}

$(document).ready(function(){
    $("#dataTable").on('click', '.btn-hapus', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        let id = $(this).data('id');

        Swal.fire({
            text: "Anda akan menghapus data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: HapusKonsultasi,
                    type:'post',
                    data:{
                        id:id,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terhapus!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        table.ajax.reload();
                    }
                })
            }
          })
    });

    $("#dataTable").on('click', '.btn-send', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        let id = $(this).data('id');
        let button = $(this);

        Swal.fire({
            text: "Anda akan mengirim data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Kirim'
          }).then((result) => {
            if (result.isConfirmed) {
                button.html('Loading..'); // Change button text to 'Loading..'
                button.attr('disabled', true); // Disable the button
                
                $.ajax({
                    url: KirimKonsultasi,
                    type:'post',
                    data:{
                        id:id,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terkirim!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        button.html('<i class="bx bx-send"></i>'); // Change button text/icon back
                        button.attr('disabled', false);
                        // table.ajax.reload();
                    }
                })
            }
          })
    });

    $("#dataTable").on('click', '.btn-edit', function(e){
        let id = $(this).data('id');
        $('.loading').show();
        $('.form-konsultasi').hide();
        $.ajax({
            url: getDataKonsultasiEdit,
            type:'get',
            dataType:'json',
            data:{
                id:id
            },
            success:function(res){
                $("#ModaltambahKonsultasi #arrGejalaInput").val(res.gejala);
            }
        })

        e.preventDefault(); // Prevent default action (e.g., following the link)

        $("#formKonsultasi").trigger('reset');

        load_konsultasi_edit(id);
        
        
        $('input[name="id"]').val(id);
        $("#ModaltambahKonsultasi").modal('show');

        $('.loading').hide();
        $('.form-konsultasi').show();
    });
});

var selectedIds = '';
function checklist(checkbox) {
    const id = $(checkbox).val();
    let idsArray = selectedIds ? selectedIds.split(',') : [];

    if ($(checkbox).is(':checked')) {
        if (!idsArray.includes(id)) {
            idsArray.push(id);
        }
    } else {
        const index = idsArray.indexOf(id);
        if (index > -1) {
            idsArray.splice(index, 1);
        }
    }

    selectedIds = idsArray.join(',');
    $('#ModaltambahKonsultasi #arrGejalaInput').val(selectedIds);
}

function checklist_edit(checkbox) {
    const id = $(checkbox).val();

    let selectedIds = $('#ModaltambahKonsultasi #arrGejalaInput').val();
    let idsArray = selectedIds ? selectedIds.split(',') : [];

    if ($(checkbox).is(':checked')) {
        if (!idsArray.includes(id)) {
            idsArray.push(id);
        }
    } else {
        const index = idsArray.indexOf(id);
        if (index > -1) {
            idsArray.splice(index, 1);
        }
    }

    selectedIds = idsArray.join(',');
    $('#ModaltambahKonsultasi #arrGejalaInput').val(selectedIds);
}


