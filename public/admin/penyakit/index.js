$(document).ready(function() {
    load_table();

    $("#tambahPenyakit").on('click',function(){
        $("#formPenyakit").trigger('reset');
        $("#ModaltambahPenyakit").modal('show');
    })
})

function load_table(){
    table = $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: dataPenyakit,
            type: 'get',
            dataType: 'json'
        },
        order: [[3, 'desc']],
        columns: [{
            data: 'DT_RowIndex'
        },
        {
            data: 'kode_penyakit',
        },
        {
            data: 'penyakit',
        },
        {
            data: 'keterangan',
        },
        {
            data: 'solusi',
        },
        {
            data: '',
            render: (data, type, row) => {
                let button = '';
                if (role == 'admin') {
                    button += `<div class="mb-1 text-center">
                        <button class="btn btn-danger btn-hapus" data-id="${row.id}"><i class="bx bx-trash"></i></button>
                        <button class="btn btn-warning btn-edit" data-id="${row.id}" data-kode_penyakit="${row.kode_penyakit}" data-penyakit="${row.penyakit}" data-keterangan="${row.keterangan}" data-solusi="${row.solusi}"><i class="bx bx-edit"></i></button>
                    </div>`;
                }
                return button;
            }
        },
        ]
    });
}

$(document).ready(function(){
    $("#dataTable").on('click', '.btn-hapus', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        let id = $(this).data('id');

        Swal.fire({
            text: "Anda akan menghapus data ini, anda yakin?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus'
          }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url:HapusGejala,
                    type:'post',
                    data:{
                        id:id,
                        _token: token
                    },
                    dataType:'json',
                    success:function(res){
                        if (res) {
                            Swal.fire(
                                'Terhapus!',
                                res.pesan,
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Gagal!',
                                res.pesan,
                                'error'
                            )
                        }
                        table.ajax.reload();
                    }
                })
            }
          })
    });

    $("#dataTable").on('click', '.btn-edit', function(e){
        e.preventDefault(); // Prevent default action (e.g., following the link)

        $("#formPenyakit").trigger('reset');
        let id = $(this).data('id');
        let penyakit = $(this).data('penyakit');
        let keterangan = $(this).data('keterangan');
        let solusi = $(this).data('solusi');
        let kode_penyakit = $(this).data('kode_penyakit');
        
        $('input[name="id"]').val(id);
        $('input[name="kode_penyakit"]').val(kode_penyakit);
        $('input[name="penyakit"]').val(penyakit);
        $('textarea[name="keterangan"]').val(keterangan);
        $('textarea[name="solusi"]').val(solusi);
        $("#ModaltambahPenyakit").modal('show');

    });
});


