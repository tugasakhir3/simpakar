<?php

use App\Http\Controllers\Auth\AuthAdmin;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::middleware('guest')->group(function () {
    Route::get('register', [RegisteredUserController::class, 'create'])->name('register');

    Route::post('register', [RegisteredUserController::class, 'store'])->name('register.store');

    Route::get('login', [AuthenticatedSessionController::class, 'create'])
                ->name('login');

    Route::post('login', [AuthenticatedSessionController::class, 'store'])->name('login_user');

    Route::get('forgot-password', [PasswordResetLinkController::class, 'create'])
                ->name('password.request');

    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
                ->name('password.email');

    Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])
                ->name('password.reset');

    Route::post('reset-password', [NewPasswordController::class, 'store'])
                ->name('password.store');
});

// Route::middleware(['auth', 'checkrole:customer'])->group(function () {
//     Route::get('/profil',[UserController::class,'profil'])->name('profil_user');
//     Route::post('/simpanprofil',[UserController::class,'simpan'])->name('simpan_profil');
//     Route::get('/jml_keranjang',[CartController::class,'jml_keranjang'])->name('jml_keranjang');
//     Route::post('/simpan_keranjang',[CartController::class,'simpan'])->name('tambah_keranjang');
//     Route::post('/hapus_keranjang',[CartController::class,'hapus'])->name('hapus_keranjang');
//     Route::post('/checkout_keranjang',[CartController::class,'checkout'])->name('checkout_keranjang');
//     Route::get('/keranjang', [CartController::class,'index'])->name('keranjang');
//     Route::get('/checkout/{id}', [CheckoutController::class,'index'])->name('checkout');
//     Route::get('/cek_ongkir', [CheckoutController::class,'cek_ongkir'])->name('cek_ongkir');
//     Route::post('/buat_pembayaran', [CheckoutController::class,'buat_pembayaran'])->name('buat_pembayaran');
//     Route::post('/terima_pesanan', [CheckoutController::class,'terima_pesanan'])->name('terima_pesanan');
//     Route::post('/upload_bukti', [CheckoutController::class,'upload_bukti'])->name('upload_bukti');
//     Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])
//     ->name('logout');
// });
