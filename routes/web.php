<?php

use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\BasisAturanController;
use App\Http\Controllers\admin\ExportController;
use App\Http\Controllers\admin\GejalaController;
use App\Http\Controllers\admin\KelolaController;
use App\Http\Controllers\admin\KonsultasiController;
use App\Http\Controllers\admin\ManajemenUserController;
use App\Http\Controllers\admin\ObatController;
use App\Http\Controllers\admin\PaketController;
use App\Http\Controllers\admin\PenyakitController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\front\FrontController;
use App\Http\Controllers\Auth\AuthAdmin;
use App\Http\Controllers\front\DetailprodukController;
use App\Http\Controllers\front\ProdukController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\EmailController;
use App\Models\Konsultasi;
use Illuminate\Support\Facades\Route;

use function Pest\Laravel\get;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// CUSTOMER

Route::get('/', [FrontController::class, 'index'])->name('home');
Route::get('/search-produk', [FrontController::class, 'search_produk']);
Route::get('/search/lihat_semua/{keyword}', [FrontController::class, 'result_produk']);

// ADMIN
Route::get('/paket', [FrontController::class, 'show_paket'])->name('show_paket');

Route::get('loginadmin', [AuthAdmin::class, 'create'])->name('loginadmin');
Route::post('loginadmin', [AuthAdmin::class, 'store']);

Route::get('get_kabupaten', [UserController::class, 'get_kabupaten'])->name('get_kabupaten');
Route::get('get_kecamatan', [UserController::class, 'get_kecamatan'])->name('get_kecamatan');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');

Route::middleware(['auth', 'checkrole:pasien'])->group(function () {
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard_user');
    Route::get('/profil', [UserController::class, 'profil'])->name('profil_user');
    Route::post('/simpanprofil', [UserController::class, 'simpan'])->name('simpan_profil');

    Route::get('/dataGejala', [GejalaController::class, 'dataGejala'])->name('dataGejalaPasien');

    Route::post('/simpan_konsultasi', [KonsultasiController::class, 'simpan'])->name('simpan_konsultasi');

});
Route::get('/cetak_konsultasi/{id}', [KonsultasiController::class, 'cetak_konsultasi'])->name('cetak_konsultasi');

Route::get('/ref_penyakit', [PenyakitController::class, 'index'])->name('penyakit');
Route::get('/dataPenyakit', [PenyakitController::class, 'dataPenyakit'])->name('dataPenyakit');

Route::middleware(['auth'])->group(function () {

    Route::get('/menu_konsultasi', [KonsultasiController::class, 'index'])->name('konsultasi');
    Route::get('/dataKonsultasi', [KonsultasiController::class, 'dataKonsultasi'])->name('dataKonsultasi');
    Route::get('/detail_konsultasi/{id}', [KonsultasiController::class, 'detail_konsultasi'])->name('detail_konsultasi');
    Route::get('/getDataKonsultasi', [KonsultasiController::class, 'getDataKonsultasi'])->name('getDataKonsultasi');

    Route::get('/tambah_konsultasi', [KonsultasiController::class, 'tambah_konsultasi'])->name('tambah_konsultasi');
    Route::get('/edit_konsultasi/{id}', [KonsultasiController::class, 'edit_konsultasi'])->name('edit_konsultasi');
    
    Route::post('/simpan_feedback', [KonsultasiController::class, 'simpan_feedback'])->name('simpanFeedback');
    Route::post('/selesai_feedback', [KonsultasiController::class, 'selesai_feedback'])->name('selesaiFeedback');
    Route::post('/hapus_konsultasi', [KonsultasiController::class, 'hapus'])->name('HapusKonsultasi');
    Route::post('/kirim_konsultasi', [EmailController::class, 'kirim'])->name('KirimKonsultasi');
    Route::get('/getDataKonsultasiEdit', [KonsultasiController::class, 'getDataKonsultasiEdit'])->name('getDataKonsultasiEdit');
});

Route::middleware(['auth', 'checkrole:admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard_admin');
        Route::get('/profil', [AdminController::class, 'profil'])->name('profil_admin');
        Route::get('/alamat_toko', [AdminController::class, 'alamat_toko'])->name('alamat_toko');
        Route::post('/simpan_profil', [AdminController::class, 'simpan_profil'])->name('simpan_profil_admin');
        Route::post('/simpan_toko', [AdminController::class, 'simpan_toko'])->name('simpan_toko');

        Route::get('/ref_obat', [ObatController::class, 'index'])->name('obat');
        Route::get('/dataObat', [ObatController::class, 'dataObat'])->name('dataObat');
        Route::post('/hapus_obat', [ObatController::class, 'hapus'])->name('HapusObat');
        Route::post('/simpan_obat', [ObatController::class, 'simpan'])->name('simpan_obat');

        Route::post('/simpan_gejala', [GejalaController::class, 'simpan'])->name('simpan_gejala');
        Route::get('/ref_gejala', [GejalaController::class, 'index'])->name('gejala');
        Route::get('/dataGejala', [GejalaController::class, 'dataGejala'])->name('dataGejala');
        Route::post('/hapus_gejala', [GejalaController::class, 'hapus'])->name('HapusGejala');
        Route::post('/simpan_gejala', [GejalaController::class, 'simpan'])->name('simpan_gejala');

        // Route::get('/ref_penyakit', [PenyakitController::class, 'index'])->name('penyakit');
        // Route::get('/dataPenyakit', [PenyakitController::class, 'dataPenyakit'])->name('dataPenyakit');
        Route::post('/hapus_penyakit', [PenyakitController::class, 'hapus'])->name('HapusPenyakit');
        Route::post('/simpan_penyakit', [PenyakitController::class, 'simpan'])->name('simpan_penyakit');

        Route::get('/basisAturan', [BasisAturanController::class, 'index'])->name('basisAturan');
        Route::get('/dataBasisAturan', [BasisAturanController::class, 'dataBasisAturan'])->name('dataBasisAturan');
        Route::post('/hapus_basis_aturan', [BasisAturanController::class, 'hapus'])->name('HapusBasisAturan');
        Route::post('/simpan_basis_aturan', [BasisAturanController::class, 'simpan'])->name('simpan_basis_aturan');

        Route::get('/getDataGejala', [BasisAturanController::class, 'getDataGejala'])->name('getDataGejala');
        Route::get('/getDataGejalaEdit', [BasisAturanController::class, 'getDataGejalaEdit'])->name('getDataGejalaEdit');

        Route::get('/getDataObat', [BasisAturanController::class, 'getDataObat'])->name('getDataObat');
        Route::get('/getDataObatEdit', [BasisAturanController::class, 'getDataObatEdit'])->name('getDataObatEdit');

        Route::get('/daftar_admin', [ManajemenUserController::class, 'daftar_admin'])->name('daftar_admin');
        Route::get('/daftar_pasien', [ManajemenUserController::class, 'daftar_pasien'])->name('daftar_pasien');
        Route::post('/tambah_admin', [ManajemenUserController::class, 'tambah_admin'])->name('tambah_admin');
        Route::post('/hapus_admin', [ManajemenUserController::class, 'hapus'])->name('hapus_admin');

        Route::get('/cetak/transaksi_bulan', [ExportController::class, 'cetak_transaksi_bulan'])->name('cetak_transaksi_bulan');
        Route::get('/cetak/cetak_transaksi', [ExportController::class, 'cetak_transaksi'])->name('cetak_transaksi');

        Route::post('/logout_admin', [AuthAdmin::class, 'destroy'])->name('logout_admin');
    });
});

Route::get('/cetak/transaksi/{id}', [ExportController::class, 'cetak'])->name('cetak_invoice');

require __DIR__ . '/auth.php';
