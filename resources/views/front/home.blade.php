@extends('front.layout.default')

@section('content')
    <section class="hero-section d-flex justify-content-center align-items-center" id="section_1">
        <div class="container">
            <div class="row">

                <div class="mx-auto col-lg-8 col-12">
                    <h1 class="text-center text-white">Diagnose. Treat. Thrive</h1>

                    <h6 class="text-center">Website untuk Edukasi dan Penanganan Penyakit Tanaman</h6>

                </div>

            </div>
        </div>
    </section>


    <section class="featured-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="custom-block custom-block-overlay">
                        <div class="h-10 d-flex flex-column">
                            <img src="{{ asset('assets_front') }}/images/plants.jpg" class="custom-block-image img-fluid"
                                alt="">

                            <div class="custom-block-overlay-text d-flex">
                                <div>
                                    <h5 class="mb-2 text-white">Tentang Website</h5>

                                    <p class="text-white">SIPAKAR dirancang bertujuan untuk memberikan edukasi
                                        pengetahuan pada masyarakat dalam memahami penyakit yang terjangkit pada
                                        tanaman serta cara penanganan dan obat yang diperlukan dalaman mengatasi
                                        penyakit tersebut. </p>

                                    @if (empty(Auth::user()->nama))
                                        <a href="{{ route('login') }}" class="mt-2 btn custom-btn mt-lg-3">Login
                                            Sekarang</a>
                                    @else
                                        <a href="{{ route('dashboard_user') }}"
                                            class="mt-2 btn custom-btn mt-lg-3">Dashboard</a>
                                    @endif
                                </div>
                            </div>
                            <div class="section-overlay"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="faq-section section-padding" id="section_4">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-12">
                    <h2 class="mb-4">Sering Ditanyakan</h2>
                </div>

                <div class="clearfix"></div>

                <div class="col-lg-5 col-12">
                    <img src="{{ asset('assets_front') }}/images/faq_graphic.jpg" class="img-fluid" alt="FAQs">
                </div>

                <div class="m-auto col-lg-6 col-12">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Apa itu SIPAKAR?
                                </button>
                            </h2>

                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <strong>SIPAKAR</strong> adalah website bertujuan untuk memberikan edukasi
                                    pengetahuan pada masyarakat dalam memahami penyakit yang terjangkit pada tanaman
                                    serta cara penanganan dan obat yang diperlukan dalaman mengatasi penyakit
                                    tersebut.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Bagaimana Cara Mendaftarkan Akun?
                                </button>
                            </h2>

                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Anda bisa kunjungi link <a href="{{ route('register') }}">disini</a>. kemudian
                                    isilah data dengan benar maka anda akan bisa menggunakan website ini. Cukup
                                    mudah bukan?
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Apakah Ini Berbayar?
                                </button>
                            </h2>

                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    Website ini bertujuan untuk membantu masyarakat luas dalam penanganan penyakit
                                    pada tanamannya dan sebisa mungkin website ini bisa bermanfaat dan bisa
                                    digunakan secara gratis.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>


    <section class="timeline-section section-padding" id="section_3">
        <div class="section-overlay"></div>

        <div class="container">
            <div class="row">

                <div class="text-center col-12">
                    <h2 class="mb-4 text-white">Bagaimana Cara Menggunakannya?</h1>
                </div>

                <div class="mx-auto col-lg-10 col-12">
                    <div class="timeline-container">
                        <ul class="vertical-scrollable-timeline" id="vertical-scrollable-timeline">
                            <div class="list-progress">
                                <div class="inner"></div>
                            </div>

                            <li>
                                <h4 class="mb-3 text-white">Daftar dan Login</h4>

                                <p class="text-white">Coba lakukan pendaftaran (jika tidak punya akun) dan lakukan
                                    login.
                                </p>

                                <div class="icon-holder">
                                    <i class="bi-person"></i>
                                </div>
                            </li>

                            <li>
                                <h4 class="mb-3 text-white">Checklist Gejala</h4>

                                <p class="text-white">Pilih dengan cara checklist gejala yang terjadi pada tanaman
                                    anda.</p>

                                <div class="icon-holder">
                                    <i class="bi-bookmark"></i>
                                </div>
                            </li>

                            <li>
                                <h4 class="mb-3 text-white">Dapatkan Solusi</h4>

                                <p class="text-white">Akan muncul hasil penyakit berdasarkan gejala yang anda
                                    pilih, penjelasan penyakit, dan cara penanganannya.</p>

                                <div class="icon-holder">
                                    <i class="bi-search"></i>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
