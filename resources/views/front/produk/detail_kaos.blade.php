@extends('front.layout.default')

@section('content')
    <div class="content">
        <div class="hero__content d-flex justify-content-center align-items-center" style="min-height: 200px;" id="home">
            <div class="title-hero">
                <h1 class="text-center fw-bold" style="vertical-align: middle;">Detail Kaos.</h1>
            </div>
        </div>

        <div class="p-5 main-product">
            <div class="content__product" id="tshirt">
                <div class="row">
                    <div class="col-6">
                        <img src="{{ asset('storage') . '/' . @$dt_produk->foto }}" class="img__detail__product"
                            style="" alt="">
                    </div>
                    <div class="col-6">
                        <div class="about-produk">
                            <div class="title-produk">
                                <span>{{ @$dt_produk->nama_produk }}</span>
                            </div>
                            <div class="price-produk">
                                <span>Rp. {{ @$dt_produk->harga }}</span>
                            </div>
                            <div class="mt-2 deskripsi-produk">
                                <span>{{ @$dt_produk->deskripsi }}</span>
                            </div>
                            <div class="mt-3 ukuran">
                                <span>Pilih Ukuran</span>
                                <select name="ukuran" id="ukuran" class="form-control" required>
                                    @foreach (@$ref_ukuran as $item)
                                        <option value="{{ $item->id }}">{{ $item->ukuran }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mt-2 jumlah">
                                <span>Jumlah Produk</span>
                                <input type="number" class="form-control" name="jumlah" id="jumlah" value="1"
                                    required>
                            </div>
                            <div class="mt-3 tombol__produk">
                                @if (Auth::user() == null)
                                    <a href="{{ url('login') }}"><button class="btn-cstm-green"><i
                                                class="fa-solid fa-cart-shopping"></i>Tambah
                                            Keranjang</button></a>
                                @else
                                    <button class="btn-cstm-green" data-id_produk="{{ @$dt_produk->id }}"
                                        id="tambah_keranjang"><i class="fa-solid fa-cart-shopping"></i>Tambah
                                        Keranjang</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
