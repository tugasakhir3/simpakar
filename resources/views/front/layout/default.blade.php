<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>SIPAKAR</title>

    <!-- CSS FILES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700&family=Open+Sans&display=swap"
        rel="stylesheet">

    <link href="{{ asset('assets_front') }}/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{ asset('assets_front') }}/css/bootstrap-icons.css" rel="stylesheet">

    <link href="{{ asset('assets_front') }}/css/templatemo-topic-listing.css" rel="stylesheet">

</head>

<body id="top">

    <main>

        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="index.html">
                    <i class="bi-back"></i>
                    <span>SIPAKAR</span>
                </a>

                <div class="d-lg-none ms-auto me-4">
                    <a href="#top" class="navbar-icon bi-person smoothscroll"></a>
                </div>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ms-lg-5 me-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_1">Home</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_4">Sering Ditanyakan</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_3">Bagaimana Cara Menggunakannya</a>
                        </li>

                    </ul>

                    @if (empty(Auth::user()->nama))
                        <div class="d-none d-lg-block">
                            <a href="{{ route('login') }}" class="navbar-icon bi-person smoothscroll"></a>
                        </div>
                    @else
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarLightDropdownMenuLink"
                                    role="button" data-bs-toggle="dropdown"
                                    aria-expanded="false">{{ Auth::user()->nama }}</a>

                                <ul class="dropdown-menu dropdown-menu-light"
                                    aria-labelledby="navbarLightDropdownMenuLink">
                                    <li><a class="dropdown-item" href="{{ url('dashboard') }}">Dashboard</a></li>

                                    <li>
                                        <form action="{{ route('logout') }}" method="post">
                                            @csrf
                                            <button type="submit" class="dropdown-item" href="contact.html"> <i
                                                    class="bi bi-logout"></i>
                                                Logout</button>
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
        </nav>

        @yield('content')

    </main>

    <footer class="site-footer section-padding">
        <div class="container">
            <div class="row">

                <div class="pb-2 mb-4 col-lg-3 col-12">
                    <a class="mb-2 navbar-brand" href="index.html">
                        <i class="bi-back"></i>
                        <span>SIPAKAR</span>
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-6">
                    <h6 class="mb-3 site-footer-title">Navigasi</h6>

                    <ul class="site-footer-links">
                        <li class="site-footer-link-item">
                            <a href="#" class="site-footer-link">Home</a>
                        </li>

                        <li class="site-footer-link-item">
                            <a href="#" class="site-footer-link">FAQs</a>
                        </li>
                        <li class="site-footer-link-item">
                            <a href="#" class="site-footer-link">How it works</a>
                        </li>
                    </ul>
                </div>

                <div class="mb-4 col-lg-3 col-md-4 col-6 mb-lg-0">
                    <h6 class="mb-3 site-footer-title">Informasi</h6>

                    <p class="mb-1 text-white d-flex">
                        <a href="tel: 305-240-9671" class="site-footer-link">
                            305-240-9671
                        </a>
                    </p>

                    <p class="text-white d-flex">
                        <a href="mailto:info@company.com" class="site-footer-link">
                            sipakar@gmail.com
                        </a>
                    </p>
                </div>

                <div class="mt-4 col-lg-3 col-md-4 col-12 mt-lg-0 ms-auto">
                    <p class="mt-4 copyright-text mt-lg-5">Copyright © 2024 SIPAKAR. All rights reserved.
                    </p>

                </div>

            </div>
        </div>
    </footer>


    <!-- JAVASCRIPT FILES -->
    <script src="{{ asset('assets_front') }}/js/jquery.min.js"></script>
    <script src="{{ asset('assets_front') }}/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('assets_front') }}/js/jquery.sticky.js"></script>
    <script src="{{ asset('assets_front') }}/js/click-scroll.js"></script>
    <script src="{{ asset('assets_front') }}/js/custom.js"></script>

</body>

</html>
