<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ @$title ?? 'SIPAKAR' }}</title>

    <link rel="shortcut icon" type="image/png" href="{{ asset('assets') }}/images/logos/favicon.png" />
    <link rel="stylesheet" href="{{ asset('assets') }}/css/styles.min.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets_login') }}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> --}}
    <!--===============================================================================================-->
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('assets_login') }}/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/css/util.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_login') }}/css/main.css"> --}}
    <!--===============================================================================================-->
</head>

<body style="background-color: #666666;">
    <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed">
        <div
            class="overflow-hidden position-relative radial-gradient min-vh-100 d-flex align-items-center justify-content-center">
            <div class="d-flex align-items-center justify-content-center w-100">
                <div class="row justify-content-center w-100">
                    <div class="col-md-8 col-lg-6 col-xxl-3">
                        <div class="mb-0 card">
                            <div class="card-body">
                                <a href="{{ url('/') }}">
                                    <h3 class="text-center"><b>SIPAKAR</b></h3>
                                </a>
                                @if (session('success'))
                                    <div class="alert alert-success">
                                        <span style="font-size: 12px;">{{ session('success') }}</span>
                                    </div>
                                @endif

                                @if (session('error'))
                                    <div class="alert alert-danger">
                                        <span style="font-size: 12px;">{{ session('error') }}</span>
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li style="font-size: 12px;">{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <form action="{{ route('register.store') }}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="exampleInputNama1" class="form-label">Nama</label>
                                                <input type="text" class="form-control" id="exampleInputNama1"
                                                    value="{{ old('nama') }}" name="nama"
                                                    aria-describedby="emailHelp">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="exampleInputUsername1" class="form-label">Username</label>
                                                <input type="text" class="form-control" id="exampleInputUsername1"
                                                    name="username" value="{{ old('username') }}"
                                                    aria-describedby="emailHelp">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="exampleInputNoHP1" class="form-label">Nomor HP</label>
                                                <input type="text" class="form-control" id="exampleInputNoHP1"
                                                    name="no_hp" value="{{ old('no_hp') }}"
                                                    aria-describedby="emailHelp">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-3">
                                                <label for="exampleInputEmail1" class="form-label">Email</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1"
                                                    name="email" value="{{ old('email') }}"
                                                    aria-describedby="emailHelp">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="mb-4">
                                                <label for="exampleInputPassword1" class="form-label">Password</label>
                                                <input type="password" name="password" class="form-control"
                                                    id="exampleInputPassword1">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-4">
                                                <label for="exampleInputKonfirmasiPassword1"
                                                    class="form-label">Konfirmasi
                                                    Password</label>
                                                <input type="password" name="confirm_password" class="form-control"
                                                    id="exampleInputKonfirmasiPassword1">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit"
                                        class="py-8 mb-4 btn btn-primary w-100 fs-4 rounded-2">Daftar</button>
                                    <div class="d-flex align-items-center justify-content-center">
                                        <p class="mb-0 fs-4 fw-bold">Sudah Punya Akun?</p>
                                        <a class="text-primary fw-bold ms-2" href="{{ route('login') }}">Login</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets') }}/libs/jquery/dist/jquery.min.js"></script>
    <script src="{{ asset('assets') }}/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('assets') }}/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    {{-- <script src="{{ asset('assets') }}/vendor/animsition/js/animsition.min.js"></script> --}}
    <!--===============================================================================================-->
    {{-- <script src="{{ asset('assets') }}/vendor/bootstrap/js/popper.js"></script> --}}
    <script src="{{ asset('assets') }}/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="{{ asset('assets') }}/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    {{-- <script src="{{ asset('assets') }}/vendor/daterangepicker/moment.min.js"></script> --}}
    <script src="{{ asset('assets') }}/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    {{-- <script src="{{ asset('assets') }}/vendor/countdowntime/countdowntime.js"></script> --}}
    <!--===============================================================================================-->
    <script src="{{ asset('assets') }}/js/main.js"></script>

</body>

</html>

</html>
