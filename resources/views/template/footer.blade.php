</div>
<div class="px-6 py-6 text-center">
    <p class="mb-0 fs-4">
    <div class="mb-2 mb-md-0">
        ©
        <script>
            document.write(new Date().getFullYear());
        </script>
        <span>Copyright &copy; SIPAKAR</span>
    </div>
    </p>
</div>
</div>
</div>
</div>
</body>

</html>


<script>
    window.imageUrl = '{{ asset('/storage/') }}';
    window.BASE_URL = '{{ url('') }}';
</script>
{{-- <script src="{{ asset('assets') }}/libs/jquery/dist/jquery.min.js"></script> --}}
<script src="{{ asset('assets') }}/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('assets') }}/js/sidebarmenu.js"></script>
<script src="{{ asset('assets') }}/js/app.min.js"></script>
<script src="{{ asset('assets') }}/libs/apexcharts/dist/apexcharts.min.js"></script>
<script src="{{ asset('assets') }}/libs/simplebar/dist/simplebar.js"></script>
<script src="{{ asset('assets') }}/js/dashboard.js"></script>

{{-- <script src="{{ asset('assets_admin') }}/vendor/js/helpers.js"></script> --}}
{{-- <script src="{{ asset('assets_admin') }}/vendor/libs/jquery/jquery.js"></script> --}}
{{-- <script src="{{ asset('assets_admin') }}/datatable/datatable.min.js"></script> --}}
{{-- <script src="{{ asset('assets_admin') }}/select2/select2.min.js"></script> --}}

<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
{{-- <script src="{{ asset('assets_admin') }}/vendor/libs/jquery/jquery.js"></script> --}}
{{-- <script src="{{ asset('assets_admin') }}/vendor/libs/popper/popper.js"></script>
<script src="{{ asset('assets_admin') }}/vendor/js/bootstrap.js"></script>
<script src="{{ asset('assets_admin') }}/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="{{ asset('assets_admin') }}/vendor/js/menu.js"></script> --}}
<!-- endbuild -->

<!-- Vendors JS -->
{{-- <script src="{{ asset('assets_admin') }}/vendor/libs/apex-charts/apexcharts.js"></script> --}}

<!-- Main JS -->
<script src="{{ asset('assets_admin') }}/js/main.js"></script>

<!-- Page JS -->
{{-- <script src="{{ asset('assets_admin') }}/js/dashboards-analytics.js"></script> --}}
{{-- <script src="{{ asset('assets_admin') }}/js/main.js"></script> --}}

<!-- Place this tag in your head or just before your close body tag. -->
{{-- <script async defer src="https://buttons.github.io/buttons.js"></script> --}}

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</body>

</html>
