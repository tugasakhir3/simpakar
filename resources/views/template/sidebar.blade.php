<style>
    /* .menu-sub .menu-link{
    background-color: #102c57;
  } */
</style>

<body>
    <!--  Body Wrapper -->
    <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed">
        <!-- Sidebar Start -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div>
                <div class="brand-logo d-flex align-items-center justify-content-between">
                    <h3><b>SIPAKAR</b></h3>

                    <div class="cursor-pointer close-btn d-xl-none d-block sidebartoggler" id="sidebarCollapse">
                        <i class="ti ti-x fs-8"></i>
                    </div>
                </div>

                @if (Auth::user()->role == 'admin')
                    @include('template/sidebar/admin')
                @else
                    @include('template/sidebar/user')
                @endif
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!--  Sidebar End -->
