@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    {{-- <div class="container-fluid" style="min-height:850px;"> --}}
    <!-- Page Heading -->
    <div class="row">
        <div class="col-sm-7">
            <div class="text-start text-sm-left">
                <div class="card-body">
                    <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}
                    </h6>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Row -->
    <div class="row">
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                        <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary" id="tambahGejala">
                            Tambah Gejala
                        </button>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-fixed table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Gejala</th>
                                    <th>Nama</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="ModaltambahGejala" tabindex="-1" aria-labelledby="ModaltambahGejalaLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <form action="{{ route('simpan_gejala') }}" method="post" id="formGejala">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModaltambahGejalaLabel">Form Gejala</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nama Gejala</label>
                            <input type="text" class="form-control" name="gejala" required>
                            <input type="hidden" class="form-control" name="id">
                        </div>
                        <div class="form-group">
                            <label for="">Kode Gejala</label>
                            <input type="text" class="form-control" name="kode_gejala" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{-- </div> --}}
    <script>
        var dataGejala = "{{ route('dataGejala') }}";
        var HapusGejala = "{{ route('HapusGejala') }}";
        var token = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('admin/gejala/index.js') }}"></script>


    <!-- End of Main Content -->
@endsection
