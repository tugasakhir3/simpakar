@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <!-- Page Heading -->
    <div class="row">
        <div class="col-sm-7">
            <div class="text-start text-sm-left">
                <div class="card-body">
                    <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}
                    </h6>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Row -->
    <div class="row">
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    <div class="container">
                        @if (session('success'))
                            <div class="alert alert-primary">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <h1 class="mb-4">Konsultasi Gejala</h1>
                        <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                            <a onclick="history.back()"
                                class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                                Kembali
                            </a>
                        </div>
                        <form method="POST" action="{{ route('simpan_konsultasi') }}">
                            @csrf
                            <input type="hidden" class="form-control" name="arr_gejala" id="arrGejalaInput">
                            @foreach ($gejala as $item)
                                <div class="mb-3">
                                    <label for="gejala{{ $item->id }}" class="form-label">Apakah gejala tanaman
                                        {{ strtolower($item->gejala) }}?</label>
                                    <select class="form-select" id="gejala{{ $item->id }}" onchange="pilihJawaban('{{ $item->id }}')"
                                        name="gejala{{ $item->id }}">
                                        <option value="">Pilih Jawaban</option>
                                        <option value="iya">Iya</option>
                                        <option value="tidak">Tidak</option>
                                    </select>
                                </div>
                            @endforeach

                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var role = "{{ Auth::user()->role }}"
        var token = '{{ csrf_token() }}';
    </script>
    <script>
        function pilihJawaban(id) {
            let gejala = $(`#gejala${id}`).val()

            let selectedIds = $('#arrGejalaInput').val();
            let idsArray = selectedIds ? selectedIds.split(',') : [];


            if (gejala == 'iya') {
                if (!idsArray.includes(id)) {
                    idsArray.push(id);
                }
                $(`#gejala${id}`).addClass("bg-success");
                $(`#gejala${id}`).removeClass("bg-danger");
            } else if (gejala == 'tidak') {
                const index = idsArray.indexOf(id);
                if (index > -1) {
                    idsArray.splice(index, 1);
                }
                $(`#gejala${id}`).addClass("bg-danger");
                $(`#gejala${id}`).removeClass("bg-success");
            } else{
                const index = idsArray.indexOf(id);
                if (index > -1) {
                    idsArray.splice(index, 1);
                }
                $(`#gejala${id}`).removeClass("bg-danger");
                $(`#gejala${id}`).removeClass("bg-success");
            }

            selectedIds = idsArray.join(',');
            $('#arrGejalaInput').val(selectedIds);
        }
    </script>


    <!-- End of Main Content -->
@endsection
