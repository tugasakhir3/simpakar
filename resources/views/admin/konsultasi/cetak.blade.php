<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Konsultasi</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }


        table {
            width: 100%;
            border-collapse: collapse;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        th,
        td {
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        tr:nth-child(even) {
            background-color: #f9f9f9;
        }
    </style>
</head>

<body>
    <div id="invoice">
        <h3 style="text-align:center;">DETAIL KONSULTASI</h3>
        <div class="form-group" style="margin-bottom:20px;">
            <label for="">Nama : {{ ucwords($dt->user->nama) }}</label>
        </div>
        <div class="form-group" style="margin-bottom:20px;">
            <label for="">Gejala-gejala yang dipilih :</label>
            <div class="table-responsive" style="margin-top: 20px;">
                <table class="table table-fixed table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Gejala</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dt->detail as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->gejala->gejala }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-group" style="margin-bottom:20px;">
            <label for="">Hasil Konsultasi Penyakit :</label>
            <div class="table-responsive" style="margin-top: 20px;">
                <table class="table table-fixed table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Penyakit</th>
                            <th>Persentase</th>
                            <th>Solusi</th>
                            <th>Obat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($hasil as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->penyakit }}</td>
                                <td>{{ ($item->gejala / $item->jml_gejala) * 100 }} %</td>
                                <td>{{ $item->solusi }}</td>
                                <td style="text-align: top;">
                                    @foreach ($item->obat as $obt)
                                        - {{ $obt->obat }}
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
