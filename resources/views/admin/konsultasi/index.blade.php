@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    {{-- <div class="container-fluid" style="min-height:850px;"> --}}
    <!-- Page Heading -->
    <div class="row">
        <div class="col-sm-7">
            <div class="text-start text-sm-left">
                <div class="card-body">
                    <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}
                    </h6>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Row -->
    <div class="row">
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (Auth::user()->role == 'pasien')
                        <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                            <a href="{{ route('tambah_konsultasi') }}"
                                class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                                Tambah Konsultasi
                            </a>
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-fixed table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ModaltambahKonsultasi" tabindex="-1" aria-labelledby="ModaltambahKonsultasiLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <form action="{{ route('simpan_konsultasi') }}" method="post" id="formKonsultasi">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModaltambahKonsultasiLabel">Form Gejala</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center loading">Loading....</div>
                        <div class="form-konsultasi" style="display: none;">
                            <div class="mb-3 form-group">
                                <input type="hidden" class="form-control" name="arr_gejala" id="arrGejalaInput">
                                <input type="hidden" class="form-control" name="id">
                            </div>
                            <div class="mb-3 form-group">
                                <label for="">Pilih Gejala</label>
                                <table class="table table-fixed table-bordered" id="dataTableKonsultasi"
                                    style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Gejala</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        var dataKonsultasi = "{{ route('dataKonsultasi') }}";
        var KirimKonsultasi = "{{ route('KirimKonsultasi') }}";
        var detailKonsultasi = "{{ url('detail_konsultasi/') }}";
    </script>

    <script>
        var dataGejala = "{{ route('dataGejalaPasien') }}";
        var getDataKonsultasi = "{{ route('getDataKonsultasi') }}";
        var getDataKonsultasiEdit = "{{ route('getDataKonsultasiEdit') }}";
        var HapusKonsultasi = "{{ route('HapusKonsultasi') }}";
        var role = "{{ Auth::user()->role }}"
        var token = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('admin/konsultasi/index.js') }}"></script>


    <!-- End of Main Content -->
@endsection
