@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    {{-- <div class="container-fluid" style="min-height:850px;"> --}}
    <!-- Page Heading -->
    <div class="row">
        <div class="col-sm-7">
            <div class="text-start text-sm-left">
                <div class="card-body">
                    <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}
                    </h6>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Row -->
    <div class="row">
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                        <a href="{{ url('cetak_konsultasi/' . $dt->id) }}" target="_blank"
                            class="shadow-sm me-3 d-none d-sm-inline-block btn btn-sm btn-primary">
                            Cetak
                        </a>
                        <a href="{{ route('konsultasi') }}"
                            class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger">
                            Kembali
                        </a>
                    </div>
                    <div class="mb-4 form-group">
                        <label for="">Nama :</label>
                        <input type="text" class="form-control" readonly value="{{ $dt->user->nama }}">
                    </div>
                    <div class="mb-4 form-group">
                        <label for="">Gejala-gejala yang dipilih :</label>
                        <div class="mt-2 table-responsive">
                            <table class="table table-fixed table-bordered" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Gejala</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dt->detail as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->gejala->gejala }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="mb-4 form-group">
                        <label for="">Hasil Konsultasi Penyakit :</label>
                        <div class="mt-2 table-responsive">
                            <table class="table table-fixed table-bordered" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Penyakit</th>
                                        <th>Persentase</th>
                                        <th>Solusi</th>
                                        <th>Obat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($hasil as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->penyakit }}</td>
                                            <td>{{ round(($item->gejala / $item->jml_gejala) * 100) }} %</td>
                                            <td>{{ $item->solusi }}</td>
                                            <td>
                                                @foreach ($item->obat as $obt)
                                                    - {{ $obt->obat }} <br>
                                                @endforeach
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <form action="{{ route('simpanFeedback') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-4 form-group">
                                    <label for="">Pertanyaan</label>
                                    <textarea name="pertanyaan" id="pertanyaan" cols="30" {{ $role == 'admin' ? 'readonly' : '' }}
                                        class="form-control" rows="5">{{ $dt->pertanyaan }}</textarea>
                                    <input type="hidden" name="id" value="{{ $dt->id }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-4 form-group">
                                    <label for="">Jawaban</label>
                                    <textarea name="jawaban" id="jawaban" cols="30" {{ $role == 'pasien' ? 'readonly' : '' }} class="form-control"
                                        rows="5">{{ $dt->jawaban }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                            @if ($role == 'pasien' && $dt->status != '1')
                                <button type="button" class="btn btn-success" onclick="selesai('{{ $dt->id }}')">Selesaikan Konsultasi</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    {{-- <div class="modal fade" id="ModaltambahGejala" tabindex="-1" aria-labelledby="ModaltambahGejalaLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <form action="{{ route('simpan_gejala') }}" method="post" id="formGejala">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModaltambahGejalaLabel">Form Gejala</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nama Gejala</label>
                            <input type="text" class="form-control" name="gejala" required>
                            <input type="hidden" class="form-control" name="id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div> --}}
    {{-- </div> --}}
    <script>
        var selesaiFeedback = "{{ route('selesaiFeedback') }}";
        // var detailKonsultasi = "{{ url('admin/detail_konsultasi/') }}";
        var token = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('admin/konsultasi/detail.js') }}"></script>


    <!-- End of Main Content -->
@endsection
