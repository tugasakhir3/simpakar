@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid" style="min-height:850px;">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3">{{ @$page_title }}</h1>
        </div>
        <!-- Content Row -->
        <div class="row">
            @if (session('success'))
                <div class="alert alert-primary">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-12">
                <form action="{{ url('/admin/simpan_profil') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="mt-3 col-12 col-md-6">
                            <label>Username</label>
                            <input type="text" class="form-control" name="username" value="{{ Auth::user()->username }}">
                        </div>
                        <div class="mt-3 col-12 col-md-6">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama" value="{{ Auth::user()->nama }}">
                        </div>
                        <div class="mt-3 col-12 col-md-6">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" value="{{ Auth::user()->email }}">
                        </div>
                        <div class="mt-3 col-12 col-md-6">
                            <label>Nomor HP</label>
                            <input type="text" class="form-control" name="no_hp" value="{{ Auth::user()->no_hp }}">
                        </div>
                        <div class="mt-3 col-4">
                            <label>Provinsi</label>
                            <select name="provinsi" id="provinsi" class="form-control select2">
                                <option value="">-- Pilih Provinsi --</option>
                                @foreach ($ref_provinsi as $v)
                                    <option value="{{ $v->province_id }}"
                                        {{ @$alamat->province_id == $v->province_id ? 'selected' : '' }}>
                                        {{ $v->province_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mt-3 col-4">
                            <label>Kabupaten</label>
                            <select name="kabupaten" id="kabupaten" class="form-control select2">
                                <option value="">-- Pilih Kabupaten --</option>
                                @if (count(@$ref_kab) > 0)
                                    @foreach ($ref_kab as $v)
                                        <option value="{{ $v->city_id }}"
                                            {{ @$alamat->city_id == $v->city_id ? 'selected' : '' }}>
                                            {{ $v->city_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="mt-3 col-4">
                            <label>Kecamatan</label>
                            <select name="kecamatan" id="kecamatan" class="form-control select2">
                                <option value="">-- Pilih Kecamatan --</option>
                                @if (count(@$ref_kec) > 0)
                                    @foreach ($ref_kec as $v)
                                        <option value="{{ $v->subdistrict_id }}"
                                            {{ @$alamat->subdistrict_id == $v->subdistrict_id ? 'selected' : '' }}>
                                            {{ $v->subdistrict_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="mt-3 col-12">
                            <label>Alamat</label>
                            <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="5">{{ @$alamat->detail_alamat }}</textarea>
                        </div>
                        <div class="mt-5 col-12">
                            <button type="submit" class="btn btn-sm btn-primary w-100">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
    <script>
        $(document).ready(function() {
            $(".select2").select2({
                theme: 'bootstrap-5'
            }) 
            $("#provinsi").change(function() {
                $("#kabupaten").html("<option>Loading</option>")
                $("#kecamatan").html(`<option value="">-- Pilih Kecamatan --</option>`)
                $.ajax({
                    url: '/get_kabupaten',
                    type: 'get',
                    dataType: 'json',
                    data: {
                        province_id: $("#provinsi").val()
                    },
                    success: function(res) {
                        $("#kabupaten").empty();
                        // Tambahkan pilihan kabupaten berdasarkan data yang diterima dari server
                        let kabupaten = '<option value="">-- Pilih Kabupaten --</option>';
                        if (res.length > 0) {
                            $.each(res, function(index, kab) {
                                kabupaten += '<option value="' + kab.city_id + '">' +
                                    kab.city_name + '</option>';
                            });
                            // $("#kabupaten").append('<option value="' + kab.city_id + '">' + kab.city_name + '</option>');
                            $("#kabupaten").append(kabupaten);
                        } else {
                            // Tampilkan pesan jika tidak ada kabupaten yang ditemukan
                            $("#kabupaten").append(
                                '<option value="">Tidak ada kabupaten ditemukan</option>');
                        }
                    }
                })
            })

            $("#kabupaten").change(function() {
                $("#kecamatan").html("<option>Loading</option>")
                $.ajax({
                    url: '/get_kecamatan',
                    type: 'get',
                    dataType: 'json',
                    data: {
                        city_id: $("#kabupaten").val()
                    },
                    success: function(res) {
                        $("#kecamatan").empty();
                        let kecamatan = '<option value="">-- Pilih kecamatan --</option>';
                        if (res.length > 0) {
                            $.each(res, function(index, kec) {
                                kecamatan += '<option value="' + kec.subdistrict_id +
                                    '">' + kec.subdistrict_name + '</option>';
                            });
                            $("#kecamatan").append(kecamatan);
                        } else {
                            $("#kecamatan").append(
                                '<option value="">Tidak ada kecamatan ditemukan</option>');
                        }
                    }
                })
            })
        })
    </script>
@endsection
