@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    {{-- <div class="container-fluid" style="min-height:850px;"> --}}
    <!-- Page Heading -->
    <div class="row">
        <div class="col-sm-7">
            <div class="text-start text-sm-left">
                <div class="card-body">
                    <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}
                    </h6>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Row -->
    <div class="row">
        <div class="mb-4 col-lg-12 order-0">
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-primary">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                        <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary" id="tambah">
                            Tambah Basis Aturan
                        </button>
                    </div>
                    <style>
                        #dataTable td {
                            font-size: 12px;
                        }
                    </style>
                    <div class="table-responsive">
                        <table class="table table-fixed table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Penyakit</th>
                                    <th>Keterangan</th>
                                    <th>Gejala</th>
                                    <th>Obat</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="Modaltambah" tabindex="-1" aria-labelledby="ModaltambahLabel" aria-hidden="true">
        <div class="p-4 modal-dialog modal-xl">
            <form action="{{ route('simpan_basis_aturan') }}" method="post" id="form">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModaltambahBasisAturanLabel">Form Basis Aturan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3 form-group">
                            <label for="">Nama Penyakit</label>
                            <select name="penyakit" id="penyakit" class="form-control">
                                <option value="">Pilih Penyakit</option>
                                @foreach ($data_penyakit as $item)
                                    <option value="{{ $item->id }}">{{ $item->penyakit }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" class="form-control" name="arr_gejala" id="arrGejalaInput">
                            <input type="hidden" class="form-control" name="arr_obat" id="arrObatInput">
                        </div>
                        <div class="mb-3 form-group">
                            <label for="">Pilih Gejala</label>
                            <table class="table table-fixed table-bordered" id="dataTableGejala" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Gejala</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                        <div class="mb-3 form-group">
                            <label for="">Pilih Obat</label>
                            <table class="table table-fixed table-bordered" id="dataTableObat" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Obat</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="Modaledit" tabindex="-1" aria-labelledby="ModaleditLabel" aria-hidden="true">
        <div class="p-4 modal-dialog modal-xl">
            <form action="{{ route('simpan_basis_aturan') }}" method="post" id="form">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModaleditBasisAturanLabel">Form Basis Aturan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center loading">Loading....</div>
                        <div class="form-edit" style="display: none;">
                            <div class="mb-3 form-group">
                                <label for="">Nama Penyakit</label>
                                <select name="penyakit" id="penyakit" class="form-control">
                                </select>
                                <input type="text" class="form-control" name="arr_gejala" id="arrGejalaInput">
                                <input type="text" class="form-control" name="arr_obat" id="arrObatInput">
                            </div>
                            <div class="mb-3 form-group">
                                <label for="">Pilih Gejala</label>
                                <table class="table table-fixed table-bordered" id="dataTableGejalaEdit"
                                    style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Gejala</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="mb-3 form-group">
                                <label for="">Pilih Obat</label>
                                <table class="table table-fixed table-bordered" id="dataTableObatEdit"
                                    style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Obat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{-- </div> --}}
    <script>
        var dataBasisAturan = "{{ route('dataBasisAturan') }}"
        var HapusBasisAturan = "{{ route('HapusBasisAturan') }}"
        var token = '{{ csrf_token() }}';

        var getDataGejala = "{{ route('getDataGejala') }}"
        var getDataGejalaEdit = "{{ route('getDataGejalaEdit') }}"
        var getDataObat = "{{ route('getDataObat') }}"
        var getDataObatEdit = "{{ route('getDataObatEdit') }}"
    </script>
    <script src="{{ asset('admin/basis_aturan/index.js') }}"></script>
    <script>
        // Initialize an empty array to store the selected IDs
        let selectedIds = '';
        let selectedIdsObat = '';

    </script>

    <!-- End of Main Content -->
@endsection
