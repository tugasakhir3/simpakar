@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid" style="min-height:850px;">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3">{{ @$page_title }}</h1>
            <a href="{{ url('admin/tambah/produk') }}" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">
                <i class="text-white fas fa-plus fa-sm fa-fw"></i> Tambah Produk</a>
        </div>

        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif

                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif

                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Produk</th>
                                            <th>Deskripsi</th>
                                            <th>Stok</th>
                                            <th>Gambar</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach ($dt_kaos as $row)
                                            <tr>
                                                <td style="width:5%!important;">{{ $no++ }}</td>
                                                <td style="width:20%!important;">{{ $row->nama_produk }}</td>
                                                <td style="width:25%!important;">{{ $row->deskripsi }}</td>
                                                <td style="width:10%!important;">{{ $row->stok }}</td>
                                                <td style="width:20%!important;">
                                                    <img src="{{ asset('storage/' . $row->foto) }}" style="width:70%;">
                                                </td>
                                                <td class="text-center" style="width:10%!important;">
                                                    <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"
                                                        data-id="{{ $row->id }}" onclick="hapus(this)"><i
                                                            class="text-white fas fa-trash fa-sm fa-fw"></i></button>
                                                    <a
                                                        href="{{ url('admin/edit/produk') . '/' . $row->id }}"class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-warning"><i
                                                            class="text-white fas fa-edit fa-sm fa-fw"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
@endsection
