@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid" style="min-height:850px;">
        <!-- Page Heading -->
        <div class="mb-4 d-sm-flex align-items-center justify-content-between">
            <h1 class="mb-0 text-gray-800 h3">{{ @$page_title }}</h1>
            <a href="{{ url('admin/kelola/kaos') }}" class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger">
                <i class="text-white fas fa-arrow-left fa-sm fa-fw"></i> Kembali</a>
        </div>
        <!-- Content Row -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="card" style="width: 100%;">
                        <div class="p-5 card-body ">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ url('admin/simpan/produk') }}" enctype="multipart/form-data" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label for="produk" class="form-label">Nama Produk</label>
                                            <input type="text" class="form-control" name="produk" id="produk"
                                                value="{{ @$produk->nama_produk }}" placeholder="Masukkan Nama Produk">
                                            @if (!empty(@$produk->id))
                                                <input type="hidden" name="id" value="{{ @$produk->id }}">
                                            @endif
                                        </div>
                                        <div class="mb-3">
                                            <label for="deskripsi" class="form-label">Deskripsi Produk</label>
                                            <textarea name="deskripsi" class="form-control" name="deskripsi" id="deskripsi" rows="3">{{ @$produk->deskripsi }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label for="harga" class="form-label">Harga Produk</label>
                                            <input type="text" class="form-control" name="harga" id="harga"
                                                value="{{ @$produk->harga }}" placeholder="Rp">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label for="kategori" class="form-label">Kategori Produk</label>
                                            <select name="kategori" id="kategori" class="form-control">
                                                <option value="">-- Pilih Kategori --</option>
                                                @foreach ($ref_kategori as $item)
                                                    <option value="{{ $item->id }}"
                                                        {{ @$produk->id_kategori == $item->id ? 'selected' : '' }}>
                                                        {{ $item->kategori }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label for="stok" class="form-label">Stok</label>
                                            <input type="text" class="form-control" name="stok" id="stok"
                                                value="{{ @$produk->stok }}" placeholder="Masukkan Stok">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        @if (!empty(@$produk->foto))
                                            {{-- <img src="{{ asset('storage') .'/'. @$produk->foto }}" style="width:70%;" class="mb-3"> --}}
                                            <a href="{{ asset('storage') . '/' . @$produk->foto }}" target="_blank"
                                                class="mb-3 shadow-sm d-none d-sm-inline-block btn btn-sm btn-success">Lihat
                                                Foto Sebelumnya</a>
                                        @endif
                                        <div class="mb-3">
                                            <label for="foto" class="form-label">Upload Foto</label>
                                            <input type="file" class="form-control" name="foto" id="foto">
                                        </div>
                                    </div>
                                    {{-- <div class="col-4">
                                        <div class="mb-3">
                                            <label for="foto" class="form-label">Ukuran Tersedia</label>
                                            <input type="text" class="form-control" name="foto" id="foto">
                                        </div>
                                    </div> --}}
                                </div>
                                <button type="submit"
                                    class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
@endsection
