@extends('template.main')

@section('content')
    <div class="container-fluid" style="min-height:850px;">
        <div class="row">
            <div class="card">
                <div class="d-flex align-items-start row">
                    <div class="col-sm-7">
                        <div class="card-body">
                            <h5 class="card-title text-primary">Selamat Datang {{ ucwords(Auth::user()->nama) }}! 🎉</h5>
                            <a href="{{ route('profil_admin') }}" class="btn btn-sm btn-outline-primary">Profil</a>
                        </div>
                    </div>
                    <div class="text-center col-sm-5 text-sm-left">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
