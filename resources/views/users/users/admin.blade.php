@extends('admin.template.main')

@section('content')
    <!-- Begin Page Content -->
    {{-- <div class="container-fluid" style="min-height:850px;"> --}}

        <!-- Page Heading -->
        {{-- <div class="row">
            <div class="col-sm-7">
                <div class="text-start text-sm-left">
                    <div class="card-body">
                        <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}</h6>
                    </div>
              </div>
            </div>
        </div> --}}
        <!-- Content Row -->
        <div class="row">
            <div class="mb-4 col-lg-12 order-0">
              @if (session('success'))
                    <div class="alert alert-primary">
                        {{ session('success') }}
                    </div>
                    @endif
                    
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif
                    
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <div class="mb-4 col-lg-12 order-0">
                <div class="card">
                    <div class="card-body">
                        <div class="p-3 pt-0 mb-3 justify-content-end d-flex">
                            <button class="shadow-sm d-none d-sm-inline-block btn btn-sm btn-primary" id="btn-tambah">
                                Tambah Admin
                            </button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-fixed table-bordered" id="dataTable" width="100%"
                                cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Nomor HP</th>
                                        {{-- <th>Alamat</th> --}}
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach ($dt_user as $row)
                                        <tr>
                                            <td style="width:5%!important;">{{ $no++ }}</td>
                                            <td style="width:20%!important;">{{ $row->username }}</td>
                                            <td style="width:20%!important;">
                                                {{ $row->nama }}
                                                {{-- <b>Email : </b>{{ $row->email }}<br>
                                                <b>Nomor HP : </b>{{ $row->no_hp }} --}}
                                            </td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->no_hp }}</td>
                                            {{-- <td style="width:30%!important;">
                                                {{ $row->detail_alamat }}
                                            </td> --}}
                                            <td class="text-center" style="width:10%!important;">
                                                @if ($jml > 1)
                                                    <button
                                                        class="p-2 shadow-sm d-none d-sm-inline-block btn btn-sm btn-danger"
                                                        onclick="hapus({{ $row->id }})"><i class="text-white fas fa-trash fa-sm fa-fw"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal-tambah" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="{{ route('tambah_admin') }}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Admin</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="username"><b>Username</b></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Username" name="username">
                                    <input type="hidden" class="form-control" placeholder="Masukkan Username" name="id" value="">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="nama"><b>Nama</b></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Nama" name="nama">
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="email"><b>Email</b></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Email" name="email">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="password"><b>Password</b></label>
                                    <input type="text" class="form-control" placeholder="Masukkan Password" name="password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End of Main Content -->
    <script src="{{ asset('admin/users/admin.js') }}"></script>  
    <script>
        $(document).ready(function(){
            $("#dataTable").DataTable()
        })
    </script>
@endsection
