<!-- Layout container -->
<style>
    .dataTables_filter {
        float: right;
    }
    .dataTables_paginate {
        float: right;
    }
</style>

<div class="layout-page">
    <!-- Navbar -->
    <nav class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
      <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
        <a class="px-0 nav-item nav-link me-xl-4" href="javascript:void(0)">
          <i class="bx bx-menu bx-sm"></i>
        </a>
      </div>

      <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
        <!-- Search -->
        <div class="navbar-nav align-items-center">
          <div class="nav-item d-flex align-items-center">
            <span>Agratha Studio | {{ @$title ?? 'Dashboard Admin' }}</span>
            {{-- <i class="bx bx-search fs-4 lh-0"></i>
            <input
              type="text"
              class="border-0 shadow-none form-control"
              placeholder="Search..."
              aria-label="Search..."
            /> --}}
          </div>
        </div>
        <!-- /Search -->

        <ul class="flex-row navbar-nav align-items-center ms-auto">
          <!-- Place this tag where you want the button to render. -->
          <!-- User -->
          <li class="nav-item navbar-dropdown dropdown-user dropdown">
            <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
              <div class="avatar avatar-online">
                <img src="{{ asset('assets_admin') }}/img/avatars/1.png" alt class="h-auto w-px-40 rounded-circle" />
              </div>
            </a>
            <ul class="dropdown-menu dropdown-menu-end">
              <li>
                <a class="dropdown-item" href="#">
                  <div class="d-flex">
                    <div class="flex-shrink-0 me-3">
                      <div class="avatar avatar-online">
                        <img src="{{ asset('assets_admin') }}/img/avatars/1.png" alt class="h-auto w-px-40 rounded-circle" />
                      </div>
                    </div>
                    <div class="flex-grow-1">
                      <span class="fw-semibold d-block">{{ ucwords(Auth::user()->nama) }}</span>
                      <small class="text-muted">Admin</small>
                    </div>
                  </div>
                </a>
              </li>
              <li>
                <div class="dropdown-divider"></div>
              </li>
              <li>
                <a class="dropdown-item" href="{{ url('/admin/profil') }}">
                  <i class="bx bx-user me-2"></i>
                  <span class="align-middle">Profil</span>
                </a>
              </li>
              <li>
                <div class="dropdown-divider"></div>
              </li>
              <li>
                <form action="{{ route('logout_admin') }}" method="post">
                  @csrf
                  <button class="dropdown-item" type="post">
                    <i class="bx bx-power-off me-2"></i>
                    <span class="align-middle">Log Out</span>
                  </button>
              </form>
              </li>
            </ul>
          </li>
          <!--/ User -->
        </ul>
      </div>
    </nav>

    <div class="content-wrapper">
      <div class="container-fluid" style="min-height:850px;">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-sm-7">
                <div class="text-start text-sm-left">
                    <div class="card-body">
                        <h6 class="fw-bold"><span class="text-muted fw-light">{{ @$menu }} /</span> {{ @$title }}</h6>
                    </div>
              </div>
            </div>
        </div>