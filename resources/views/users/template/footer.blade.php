 <!-- Footer -->
 <footer class="content-footer footer bg-footer-theme">
    <div class="flex-wrap py-2 container-xxl d-flex justify-content-between flex-md-row flex-column">
      <div class="mb-2 mb-md-0">
        ©
        <script>
          document.write(new Date().getFullYear());
        </script>
        <span>Copyright &copy; Agratha Studio 2024</span>
      </div>
    </div>
  </footer>
  <!-- / Footer -->

  <div class="content-backdrop fade"></div>
</div>
<!-- Content wrapper -->
</div>
<!-- / Layout page -->
</div>

<!-- Overlay -->
<div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->

{{-- <div class="buy-now">
<a
href="https://themeselection.com/products/sneat-bootstrap-html-admin-template/"
target="_blank"
class="btn btn-danger btn-buy-now"
>Upgrade to Pro</a
>
</div> --}}
<script>
  window.imageUrl = '{{ asset("/storage/") }}';  
  window.BASE_URL = '{{ url("") }}';  

</script>

<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
{{-- <script src="{{ asset('assets_admin') }}/vendor/libs/jquery/jquery.js"></script> --}}
<script src="{{ asset('assets_admin') }}/vendor/libs/popper/popper.js"></script>
<script src="{{ asset('assets_admin') }}/vendor/js/bootstrap.js"></script>
<script src="{{ asset('assets_admin') }}/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="{{ asset('assets_admin') }}/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="{{ asset('assets_admin') }}/vendor/libs/apex-charts/apexcharts.js"></script>

<!-- Main JS -->
<script src="{{ asset('assets_admin') }}/js/main.js"></script>

<!-- Page JS -->
<script src="{{ asset('assets_admin') }}/js/dashboards-analytics.js"></script>
<script src="{{ asset('assets_admin') }}/js/main.js"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</body>
</html>
